import type { PacketType } from '../PacketType';
import type { AbstractPacketHandler } from './AbstractPacketHandler';
import { ChatPacketHandler } from './ChatPacketHandler';
import { FightPacketHandler } from './FightPacketHandler';
import { InventoryPacketHandler } from './InventoryPacketHandler';
import { LoginPacketHandler } from './LoginPacketHandler';
import { MovingPacketHandler } from './MovingPacketHandler';
import { NpcPacketHandler } from './NpcPacketHandler';
import { PetPacketHandler } from './PetPacketHandler';
import { PlayerDataPacketHandler } from './PlayerDataPacketHandler';
import { PreGamePacketHandler } from './PreGamePacketHandler';
import { QuestPacketHandler } from './QuestPacketHandler';
import { StorePacketHandler } from './StorePacketHandler';

/**
 * Stores all packet handlers.
 */
export class PacketHandlerCollection{
	private handlers: AbstractPacketHandler[];
	
	public constructor(){
		this.handlers = [
			new ChatPacketHandler(),
			new MovingPacketHandler(),
			new PreGamePacketHandler(),
			new PlayerDataPacketHandler(),
			new QuestPacketHandler(),
			new NpcPacketHandler(),
			new FightPacketHandler(),
			new PetPacketHandler(),
			new InventoryPacketHandler(),
			new StorePacketHandler(),
			new LoginPacketHandler(),
		];
	}
	
	/**
	 * Find the packet handler for the given type.
	 * @param type
	 */
	public getPacketHandler(type: PacketType): AbstractPacketHandler | null{
		for(let handler of this.handlers){
			if(handler.handlesType(type))
				return handler;
		}
		
		return null;
	}
}
