import type { ClientConnection } from '../ClientConnection';
import { getPacketType } from '../PacketReader';
import { PacketType } from '../PacketType';
import { AbstractPacketHandler } from './AbstractPacketHandler';

/**
 * Handles chat packets.
 */
export class ChatPacketHandler extends AbstractPacketHandler{
	/**
	 * Checks if this packet handler is used to handle a packet type.
	 * @param type
	 */
	public handlesType(type: PacketType): boolean{
		return type >= 0x00040074 && type < 0x00050000;
	}
	
	/**
	 * Handles the given packet.
	 * @param packet
	 * @param client
	 */
	public handlePacket(packet: Buffer, client: ClientConnection): void{
		let type = getPacketType(packet);
		
		switch(type){
			case PacketType.ChatLocalBC:
				packet.writeUInt32LE(PacketType.ChatLocalBS, 8);
				client.write(packet);
				break;
			default:
				this.notImplemented(packet);
		}
	}
}
