import type { ClientConnection, ClientConnectionWithPlayer } from '../ClientConnection';
import type { CharacterGender, CharacterRace } from '../Enums/CharacterClass';
import { Player } from '../GameState/Player/Player';
import { getPacketType } from '../PacketReader';
import { PacketType } from '../PacketType';
import { characterChoiceConfirm, characterChoiceError } from '../Responses/Pregame/CharacterChoice';
import { characterCreateConfirm, characterCreateNameError } from '../Responses/Pregame/CharacterCreate';
import { getCharacterList } from '../Responses/Pregame/CharacterList';
import { chatServerInfoPacket } from '../Responses/Pregame/ChatServerInfo';
import { keepAlivePacket } from '../Responses/Pregame/KeepAlive';
import { versionPacket } from '../Responses/Pregame/Version';
import { endAtZero, isValidName } from '../Utils/StringUtils';
import { AbstractPacketHandler } from './AbstractPacketHandler';

/**
 * Handles packets that happen before the player enters the game world.
 */
export class PreGamePacketHandler extends AbstractPacketHandler{
	/**
	 * Checks if this packet handler is used to handle a packet type.
	 * @param type
	 */
	public handlesType(type: PacketType): boolean{
		return type === PacketType.KeepAlive
			|| type === PacketType.Version
			|| type === PacketType.CharacterList
			|| type === PacketType.CharacterChoice
			|| type === PacketType.CharacterCreate;
	}
	
	/**
	 * Handles the given packet.
	 * @param packet
	 * @param client
	 */
	public handlePacket(packet: Buffer, client: ClientConnection): void{
		let type = getPacketType(packet);
		
		switch(type){
			// Version-check
			case PacketType.Version:
				client.write(versionPacket);
				break;
			
			// Pinging, answering is not always required
			case PacketType.KeepAlive:
				client.write(keepAlivePacket);
				break;
			
			// Retrieve a character list
			case PacketType.CharacterList:
				client.write(getCharacterList(client.user!));
				break;
			
			// The client picked a character
			case PacketType.CharacterChoice:
				this.onCharacterChoice(packet, client);
				break;
			
			// The client creates a character
			case PacketType.CharacterCreate:
				this.onCharacterCreate(packet, client);
				break;
			
			default:
				this.notImplemented(packet);
		}
	}
	
	/**
	 * Respond to the player's character choice.
	 * @param packet
	 * @param client
	 */
	private onCharacterChoice(packet: Buffer, client: ClientConnection): void{
		let id = packet.readUInt8(12);
		let player = client.user?.characters.find(c => c.id === id);
		
		// TODO check if user is already logged in
		if(!player){
			client.write(characterChoiceError);
			return;
		}
		
		this.loginAsCharacter(client, player);
	}
	
	/**
	 * Handle character creation request.
	 * @param packet
	 * @param client
	 */
	private onCharacterCreate(packet: Buffer, client: ClientConnection): void{
		if(client.player || !client.user || client.user.characters.length >= 5)
			return;
		
		let race: CharacterRace = packet.readUInt8(35);
		let gender: CharacterGender = packet.readUInt8(36);
		
		if(race > 3 || gender > 1)
			return;
		
		let name = packet.toString('ascii', 20, 34);
		name = endAtZero(name);
		
		// TODO check if name is taken.
		if(!isValidName(name)){
			client.write(characterCreateNameError);
			return;
		}
		
		let player = new Player(client.game, client);
		player.race = race;
		player.gender = gender;
		player.name = name;
		player.id = 124;
		
		player.mapData.map = client.game.maps.get(1)!;
		player.mapData.x = player.mapData.destX = 475 * 16;
		player.mapData.y = player.mapData.destY = 250 * 8;
		
		player.useDefaultFile();
		player.fightData.stats.healHp();
		player.fightData.stats.healMp();
		
		client.user.characters.push(player);
		client.write(characterCreateConfirm);
		this.loginAsCharacter(client, player);
	}
	
	/**
	 * Logs the user in as the given character.
	 * @param client
	 * @param player
	 */
	private loginAsCharacter(client: ClientConnection, player: Player): void{
		client.player = player;
		player.client = client as ClientConnectionWithPlayer;
		client.game.addPlayer(player);
		
		client.write(characterChoiceConfirm);
		client.write(chatServerInfoPacket);
		
		// TODO check if character is in fight
		player.enterMap(true);
	}
}
