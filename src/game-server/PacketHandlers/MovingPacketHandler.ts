import type { ClientConnection, ClientConnectionWithPlayer } from '../ClientConnection';
import { getPacketType } from '../PacketReader';
import { PacketType } from '../PacketType';
import { MapPackets } from '../Responses/MapPackets';
import { AbstractPacketHandler } from './AbstractPacketHandler';

/**
 * Handles packets regarding the movement of the player.
 */
export class MovingPacketHandler extends AbstractPacketHandler{
	/**
	 * Checks if this packet handler is used to handle a packet type.
	 * @param type
	 */
	public handlesType(type: PacketType): boolean{
		return type === PacketType.PlayerMove
			|| type === PacketType.WalkDestinations
			|| type === PacketType.UpdateCoordinates;
	}
	
	/**
	 * Handles the given packet.
	 * @param packet
	 * @param client
	 */
	public handlePacket(packet: Buffer, client: ClientConnection): void{
		if(!this.hasPlayer(client))
			return;
		
		let type = getPacketType(packet);
		
		switch(type){
			case PacketType.PlayerMove:
				this.onPlayerMove(packet, client);
				break;
			default:
				this.notImplemented(packet);
		}
	}
	
	/**
	 * Update the game state when the player moves.
	 * @param packet
	 * @param player
	 */
	private onPlayerMove(packet: Buffer, client: ClientConnectionWithPlayer): void{
		if(!client.player.mapData.map)
			return;
		
		let id = packet.readUInt32LE(16);
		
		if(id !== client.player.id)
			return;
		
		let x = packet.readUInt16LE(20);
		let y = packet.readUInt16LE(22);
		
		/*
		uint16 24: always 1?
		uint16 26: 0, 70 or 168
			0 when clicking somewhere
			70 when keeping mouse down while moving around
			168 when "swiping", mouse down on one spot, quickly moving it elsewhere, and letting go
			You often get a 168 before a series of 70
		*/
		
		// TEMP
		let link = client.player.mapData.map.getMapLink(x, y);
		if(link?.dest){
			console.log(link);
			link.dest.addPlayer(client.player);
			[x, y] = link.getDestinationCoordinates();
		}
		
		client.player.mapData.destX = x;
		client.player.mapData.destY = y;
		client.player.mapData.x = x;
		client.player.mapData.y = y;
		
		if(link?.dest)
			client.player.enterMap();
		else
			client.write(MapPackets.walkDestinations(client.player));
	}
}
