import type { ClientConnection, ClientConnectionWithPlayer } from '../ClientConnection';
import { getPacketType } from '../PacketReader';
import { PacketType } from '../PacketType';

export abstract class AbstractPacketHandler{
	/**
	 * Checks if this packet handler is used to handle a packet type.
	 * @param type
	 */
	public abstract handlesType(type: PacketType): boolean;
	
	/**
	 * Handles the given packet.
	 * @param packet
	 * @param client
	 */
	public abstract handlePacket(packet: Buffer, client: ClientConnection): void;
	
	/**
	 * Warn when a packet is not yet implemented.
	 * @param packet
	 */
	protected notImplemented(packet: Buffer): void{
		let type = getPacketType(packet);
		console.warn('Unhandled packet type ' + type.toString(16) + ' ' + PacketType[type]);
	}
	
	/**
	 * Check if the client has a player.
	 * @param client
	 */
	protected hasPlayer(client: ClientConnection): client is ClientConnectionWithPlayer{
		return client.player !== null;
	}
}
