import type { Socket } from 'net';
import { createServer } from 'net';
import { ClientConnection } from './ClientConnection';
import type { Game } from './GameState/Game';
import { PacketHandlerCollection } from './PacketHandlers/PacketHandlerCollection';

/**
 * The server class that holds the game state and listens to new connections.
 */
export class GameServer{
	/**
	 * The global game state.
	 */
	public game: Game;
	
	/**
	 * The classes that deal with game packets.
	 */
	public packetHandlers: PacketHandlerCollection;
	
	/**
	 * All open client connections.
	 */
	public connections: ClientConnection[] = [];
	
	public constructor(host: string, port: number, game: Game){
		this.game = game;
		this.packetHandlers = new PacketHandlerCollection();
		let server = createServer();
		
		server.on('connection', socket => this.onConnection(socket));
		server.on('close', () => this.onClose());
		server.on('error', err => this.onError(err));
		server.listen(port, host);
	}
	
	/**
	 * Handle a new client connection.
	 * @param socket
	 */
	private onConnection(socket: Socket): void{
		let connection = new ClientConnection(socket, this);
		this.connections.push(connection);
	}
	
	/**
	 * Called when the server closes.
	 */
	private onClose(): void{
		console.log('Server was closed');
	}
	
	/**
	 * Called when an error happens in the tcp server.
	 * @param err
	 */
	private onError(err: Error): void{
		console.log('Server error', err);
	}
}
