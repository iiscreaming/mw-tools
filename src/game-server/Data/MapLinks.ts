import { readFileSync } from 'fs';

export type MapFileJson = {
	map: number,
	width: number,
	height: number,
	links: MapLinkJson[],
};

export type MapLinkJson = {
	id: number,
	map: number,
	x1: number,
	y1: number,
	x2: number,
	y2: number,
};

// TODO store this somewhere reasonable
let linksFile = module.path + '/../../../output/map/links.json';
let cellDir = module.path + '/../../../output/map/cells/';

function getCellData(map: number): Buffer{
	return readFileSync(cellDir + '/' + map + '.bin');
}

let json = readFileSync(linksFile, {encoding: 'ascii'});
let links: MapFileJson[] = JSON.parse(json);
let linkMap = new Map(links.map(link => [link.map, link]));
let cellData = new Map(links.map(link => [link.map, getCellData(link.map)]));

export const mapLinkData = linkMap;
export const mapCellData = cellData;
