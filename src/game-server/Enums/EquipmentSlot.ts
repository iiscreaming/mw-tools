export enum EquipmentSlot{
	Head		= 0,
	Armour		= 1,
	Weapon		= 2,
	Shoes		= 3,
	Necklace	= 4,
	Bracelet	= 5,
	Ring		= 6,
}
