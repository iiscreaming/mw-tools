export enum Weather{
	Sunny = 0,
	LightRain = 1,
	LightSnow = 2,
	Clouds = 4,
	Thunderstorm = 8,
}
