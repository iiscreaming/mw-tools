export enum Species{
	Empty	= 0,
	Demon	= 1,
	Flying	= 2,
	Special	= 3,
	Undead	= 4,
	Human	= 5,
	Dragon	= 6,
}
