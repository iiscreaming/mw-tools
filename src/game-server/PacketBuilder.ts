/**
 * Creates a packet for the given size. Fills in the first 8 bytes.
 * @param size
 */
export function createPacket(size: number): Buffer{
	if(size < 12)
		throw Error('Packet too small.');
	
	let arr = Buffer.alloc(size, 0, 'binary');
	
	// Required header
	arr[0] = 0x55;
	arr[1] = 0x47;
	
	// Packet length - 4
	arr.writeUInt16LE(size - 4, 2);
	
	/* Mysterious bytes.
	 * Sometimes these have to be 0, or anything higher than 0,
	 * sometimes a specific value, and other times used as loop count.
	 * They might be read as two seperate 16 bit ints, or as one 32 bit int.
	 * Sometimes used as signed, sometimes as unsigned.
	 * Ideally these would default as 0 and set specifically for each packet.
	 * Setting them lower may slightly increase client performance when used in a loop.
	 * Currently set to the maximum signed values, to make testing easier.
	 * When setting them too low, sometimes a part of the packet is ignored,
	 * which is really annoying when debugging.
	 * If a packet is ever giving issues, look into changing these bytes.
	 */
	arr.writeUInt32LE(0x7FFF7FFF, 4);
	
	return arr;
}
