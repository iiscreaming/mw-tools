import { npcText } from '../../Data/NpcText';
import type { NpcJson } from '../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../Enums/Direction';
import { NpcId } from '../../Enums/NpcId';

export const npcDataList: NpcJson[] = [
	{
		id: NpcId.WoodTele,
		name: 'Teleporter',
		file: 120,
		map: 1,
		x: 7808,
		y: 2110,
		direction: Direction.SouthWest,
		action: {
			type: 'npcSay',
			message: npcText.teleporter,
			options: [
				{
					text: '#GBlython (0 gold)#E',
					action: {
						type: 'teleport',
						targetNpcId: NpcId.BlythonTele,
					},
				},
				{
					text: '#GTest Fight#E',
					action: {type: 'template', template: 'testFight'},
				},
				{
					text: '#GQuest?#E',
					condition: {
						type: 'quest',
						not: true,
						quest: 1,
					},
					action: {
						type: 'quest',
						add: {
							quest: 1,
						},
					},
				},
				{
					text: '#YClose#E',
				},
			],
		},
	},
	{
		id: NpcId.BlythonTele,
		name: 'Teleporter',
		file: 120,
		map: 2,
		x: 1365,
		y: 5180,
		direction: Direction.SouthEast,
		action: {
			type: 'npcSay',
			message: npcText.teleporter,
			options: [
				{
					text: '#GWoodlingor (0 gold)#E',
					action: {
						type: 'teleport',
						targetNpcId: NpcId.WoodTele,
					},
				},
				{
					text: '#GTest Shop#E',
					action: {type: 'template', template: 'testShop'},
				},
				{
					text: '#GQuest?#E',
					condition: {
						type: 'quest',
						quest: 1,
					},
					action: {
						type: 'npcSay',
						message: 'This is the end of the quest, wasn\'t that exciting? #51',
						onClose: {
							type: 'quest',
							remove: 1,
						},
					},
				},
				{
					text: '#YClose#E',
				},
			],
		},
	},
	{
		id: 0x80000003,
		name: 'Exp Test',
		file: 130,
		direction: Direction.SouthEast,
		map: 1,
		x: 7500,
		y: 2000,
		action: {
			type: 'npcSay',
			options: [
				{
					text: '#GPlayer Exp#E',
					action: {type: 'exp', exp: 1000},
				},
				{
					text: '#GPet Exp#E',
					action: {type: 'exp', exp: 500, pet: true},
				},
				{
					text: '#GHeal Pet#E',
					action: {type: 'heal', hp: 20, pet: true},
				},
			],
		},
	},
];
