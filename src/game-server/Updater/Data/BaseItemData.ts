import type { BaseItemJson } from '../../Database/Collections/BaseItem/BaseItemTypes';
import { EquipmentSlot } from '../../Enums/EquipmentSlot';
import { ItemType } from '../../GameState/Item/ItemType';

export const baseItemList: BaseItemJson[] = [
	{
		id: 1,
		file: 126,
		name: 'Sword of Whatever',
		description: 'Pointy#E #E#G+5 Boredom',
		type: ItemType.Equipment,
		equipmentSlot: EquipmentSlot.Weapon,
		stackLimit: 1,
		action: null,
	},
	{
		id: 2,
		file: 0,
		name: 'Apple',
		description: 'Heals 10 hp',
		stackLimit: 30,
		type: ItemType.Consumable,
		action: {type: 'heal', hp: 10},
		equipmentSlot: null,
	},
	{
		id: 3,
		file: 1,
		name: 'Banana',
		description: 'Heals 25 hp',
		stackLimit: 30,
		type: ItemType.Consumable,
		action: {type: 'heal', hp: 25},
		equipmentSlot: null,
	},
];
