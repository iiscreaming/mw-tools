import type { MapData } from '../../Database/Collections/GameData/types';

export const mapDataList: MapData[] = [
	{
		id: 1,
		name: 'Woodlingor',
		file: 3,
		musicFile: 0,
		minimapFile: 2,
		fightMusicFile: 0,
		fightBackgroundFile: 18,
	},
	{
		id: 2,
		name: 'Blython',
		file: 2,
		musicFile: 0,
		minimapFile: 1,
		fightMusicFile: 0,
		fightBackgroundFile: 19,
	},
	{
		id: 3,
		name: 'Revive Arena',
		file: 75,
		musicFile: 0,
		minimapFile: 13,
		fightMusicFile: 0,
		fightBackgroundFile: 20,
	},
];
