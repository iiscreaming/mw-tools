export const bucketSettings: couchbase.CreateBucketSettings[] = [
	{
		bucketType: couchbase.BucketType.Couchbase,
		conflictResolutionType: couchbase.ConflictResolutionType.SequenceNumber,
		ejectionMethod: couchbase.EvictionPolicy.ValueOnly,
		flushEnabled: false,
		minimumDurabilityLevel: couchbase.DurabilityLevel.None,
		name: 'MythWarServer',
		numReplicas: 0,
		ramQuotaMB: 256,
		replicaIndexes: false,
	},
];
