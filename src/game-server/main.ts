import 'source-map-support/register';
import { Config } from './Config/Config';
import { GameServer } from './GameServer';
import { Game } from './GameState/Game';
import { enableGlobalErrorCatching } from './Logger/Logger';

enableGlobalErrorCatching();

let {world, chat} = Config.get();

(async() => {
	let game = new Game();
	await game.init();
	
	// World
	new GameServer(world.ip, world.port, game);
	
	// Chat
	new GameServer(chat.ip, chat.port, game);
})();
