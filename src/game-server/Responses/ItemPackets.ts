import type { EquipmentSlot } from '../Enums/EquipmentSlot';
import type { Item } from '../GameState/Item/Item';
import type { ItemContainer } from '../GameState/Item/ItemContainer';
import type { PlayerItems } from '../GameState/Player/PlayerItems';
import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';
import { ItemStruct } from './Structs/ItemStruct';

export enum ItemEquipStatus{
	Ok			= 1,
	LowInt		= -1,
	LowStr		= -2,
	LowAgi		= -3,
	LowSta		= -4,
	LowLvl		= -5,
	WrongRace	= -6,
}

export class ItemPackets{
	/**
	 * Send the player's inventory to the client.
	 * @param inventory
	 */
	public static inventory(inventory: ItemContainer): Buffer{
		let packet = createPacket(16 + inventory.usedSize * ItemStruct.size);
		packet.writeUInt32LE(PacketType.InventoryData, 8);
		packet.writeUInt8(inventory.usedSize, 12);
		ItemStruct.writeList(packet, inventory.entries(), 16);
		return packet;
	}
	
	// TODO is this used?
	// public static add(): Buffer{}
	
	/**
	 * Remove an item from the client inventory.
	 * @param index slot number
	 */
	public static remove(index: number): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.ItemRemove, 8);
		packet.writeUInt8(index, 12);
		return packet;
	}
	
	/**
	 * Update specific slots in the client's inventory.
	 * Null empties the slot.
	 * TODO: slot >= 0x01000000 targets different client memory area, maybe shop or bank?
	 * @param items
	 */
	public static change(items: [slot: number, item: Item | null][]): Buffer{
		let packet = createPacket(16 + items.length * ItemStruct.size);
		packet.writeUInt32LE(PacketType.ItemChange, 8);
		packet.writeUInt8(items.length, 12);
		ItemStruct.writeList(packet, items, 16);
		return packet;
	}
	
	/**
	 * Update the client's gold (both inventory and bank).
	 * @param playerItems
	 */
	public static gold(playerItems: PlayerItems): Buffer{
		let packet = createPacket(24);
		packet.writeUInt32LE(PacketType.GoldData, 8);
		packet.writeUInt32LE(playerItems.gold, 16);
		packet.writeUInt32LE(playerItems.bankGold, 20);
		return packet;
	}
	
	// TODO
	// public static give(): Buffer{}
	
	/**
	 * Update a single equipment slot.
	 * @param status
	 * @param slot
	 * @param item
	 */
	public static equip(status: ItemEquipStatus, slot: EquipmentSlot, item: Item | null): Buffer{
		let packet = createPacket(16 + ItemStruct.size);
		packet.writeUInt32LE(PacketType.ItemEquip, 8);
		packet.writeInt32LE(status, 12);
		ItemStruct.write(packet, slot, item, 16);
		return packet;
	}
	
	/**
	 * Update all equipment slots.
	 * @param equipment
	 */
	public static equipment(equipment: ItemContainer): Buffer{
		let packet = createPacket(16 + equipment.usedSize * ItemStruct.size);
		packet.writeUInt32LE(PacketType.EquipmentList, 8);
		packet.writeUInt8(equipment.usedSize, 12);
		ItemStruct.writeList(packet, equipment.entries(), 16);
		return packet;
	}
	
	/**
	 * Send the text of an item description.
	 * @param item
	 */
	public static info(item: Item): Buffer{
		let text = item.getText();
		let packet = createPacket(16 + text.length + 1);
		packet.writeUInt32LE(PacketType.ItemInfo, 8);
		// 12 must be empty
		packet.write(text, 16);
		return packet;
	}
}
