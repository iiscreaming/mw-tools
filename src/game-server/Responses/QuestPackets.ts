import type { PlayerQuest } from '../GameState/Quest/PlayerQuest';
import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';

export class QuestPackets{
	public static readonly MaxSituationLength: number = 120;
	public static readonly MaxRequirementsLength: number = 250;
	public static readonly MaxRewardLength: number = 250;
	
	/**
	 * Add a quest to the player's log.
	 * @param playerQuest
	 */
	public static add({quest, stage}: PlayerQuest): Buffer{
		let situationLen = stage.situation.length + 1;
		let requirementLen = stage.requirements.length + 1;
		let rewardLen = stage.reward.length + 1;
		let length = 24 + situationLen + requirementLen + rewardLen;
		
		let packet = createPacket(length);
		packet.writeUInt16LE(length - 16, 6);
		packet.writeUInt32LE(PacketType.QuestAdd, 8);
		packet.writeUInt32LE(quest.clientId, 16);
		packet.writeUInt8(situationLen, 20);
		packet.writeUInt8(requirementLen, 21);
		packet.writeUInt8(rewardLen, 22);
		
		if(stage.situation.length !== 0)
			packet.write(stage.situation, 24);
		
		if(stage.requirements.length !== 0)
			packet.write(stage.requirements, 24 + situationLen);
		
		if(stage.reward.length !== 0)
			packet.write(stage.reward, 24 + situationLen + requirementLen);
		
		return packet;
	}
	
	/**
	 * Update the situation text of this quest.
	 * @param playerQuest
	 */
	public static updateSituation({quest, stage}: PlayerQuest): Buffer{
		let packet = createPacket(16 + stage.situation.length + 1);
		packet.writeUInt32LE(PacketType.QuestUpdateSituation, 8);
		packet.writeUInt32LE(quest.clientId, 12);
		packet.write(stage.situation, 16);
		
		return packet;
	}
	
	/**
	 * Update the requirements text of this quest.
	 * @param playerQuest
	 */
	public static updateRequirements({quest, stage}: PlayerQuest): Buffer{
		let packet = createPacket(16 + stage.requirements.length + 1);
		packet.writeUInt32LE(PacketType.QuestUpdateRequirements, 8);
		packet.writeUInt32LE(quest.clientId, 12);
		packet.write(stage.requirements, 16);
		
		return packet;
	}
	
	/**
	 * Update the reward text of this quest.
	 * @param playerQuest
	 */
	public static updateReward({quest, stage}: PlayerQuest): Buffer{
		let packet = createPacket(16 + stage.reward.length + 1);
		packet.writeUInt32LE(PacketType.QuestUpdateReward, 8);
		packet.writeUInt32LE(quest.clientId, 12);
		packet.write(stage.reward, 16);
		
		return packet;
	}
	
	/**
	 * Remove the quest from the player's log.
	 * @param playerQuest
	 */
	public static remove({quest}: PlayerQuest): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.QuestRemove, 8);
		packet.writeUInt32LE(quest.clientId, 12);
		
		return packet;
	}
	
	/**
	 * Clears the situation, requirements and rewards text.
	 * @param playerQuest
	 */
	public static clearText({quest}: PlayerQuest): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.QuestClearText, 8);
		packet.writeUInt32LE(quest.clientId, 12);
		
		return packet;
	}
}
