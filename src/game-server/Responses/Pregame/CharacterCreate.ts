import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

export const characterCreateConfirm = createPacket(16);
characterCreateConfirm.writeUInt32LE(PacketType.CharacterCreate, 8);

export const characterCreateNameExists = createPacket(16);
characterCreateNameExists.writeUInt32LE(PacketType.CharacterCreate, 8);
characterCreateNameExists.writeInt32LE(-2, 12);

export const characterCreateNameError = createPacket(16);
characterCreateNameError.writeUInt32LE(PacketType.CharacterCreate, 8);
characterCreateNameError.writeInt32LE(-1, 12);
