import { Config } from '../../Config/Config';
import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

let packet = createPacket(22);
packet.writeUInt32LE(PacketType.ChatServerInfo, 8);

let config = Config.get();
let ip = config.chat.ip.split('.').map(s => Number.parseInt(s));
let port = config.chat.port;

packet.writeUInt8(ip[0], 16);
packet.writeUInt8(ip[1], 17);
packet.writeUInt8(ip[2], 18);
packet.writeUInt8(ip[3], 19);
packet.writeUInt16LE(port, 20);

export const chatServerInfoPacket = packet;
