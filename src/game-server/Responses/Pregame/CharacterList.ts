import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';
import type { User } from '../../GameState/User';

export function getCharacterList(user: User): Buffer{
	let packet = createPacket(16 + 28 * user.characters.length);
	
	// Type
	packet.writeUInt32LE(PacketType.CharacterList, 8);
	
	// Character count
	packet.writeUInt16LE(user.characters.length, 12);
	
	for(let i = 0; i < user.characters.length; ++i){
		let char = user.characters[i];
		let offset = 16 + i * 28;
		// ID (0-3)
		packet.writeUInt32LE(char.id, offset);
		// Name (4-15)
		packet.write(char.name, offset + 4);
		// Skip 3 (16-18)
		// Class (19)
		packet.writeUInt8(char.race, offset + 19);
		// Gender (20)
		packet.writeUInt8(char.gender, offset + 20);
		// Skip 1 (21)
		// Reborn (22)
		packet.writeUInt8(char.level.reborn, offset + 22);
		// Skip 1 (23)
		// Level (24-25)
		packet.writeUInt16LE(char.level.level, offset + 24);
		// Skip 2 (26-27)
	}
	
	return packet;
}
