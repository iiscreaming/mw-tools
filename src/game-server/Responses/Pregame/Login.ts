import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

enum LoginType{
	Fail = 0,
	Success = 4,
}

enum LoginFailType{
	None = 0,
	// Deprecated: WrongUser = 1, WrongPass = 2
	WrongUserOrPass = 3,
	NotActivated = 4,
	AccountBlocked = 5,
	IpBlocked = 6,
}

let packet = createPacket(20);
packet.writeUInt32LE(PacketType.LoginResponse, 8);
packet.writeUInt16LE(LoginType.Success, 6);
packet.writeUInt32LE(LoginFailType.None, 12);
packet.writeUInt32LE(3600, 16); // Expiry?

export const testSuccess = packet;
