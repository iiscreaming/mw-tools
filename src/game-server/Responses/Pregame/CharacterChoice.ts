import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

export const characterChoiceConfirm = createPacket(16);
characterChoiceConfirm.writeUInt32LE(PacketType.CharacterChoice, 8);

export const characterChoiceError = createPacket(16);
characterChoiceError.writeUInt32LE(PacketType.CharacterChoice, 8);
characterChoiceError.writeInt32LE(-1, 12);
