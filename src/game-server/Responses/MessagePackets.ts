import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';

enum MessageType{
	Error = 0,
	Message = 1,
	System = 2,
}

export class MessagePackets{
	/**
	 * Shows a blue message in the middle of the screen.
	 * @param message
	 */
	public static showMessage(message: string): Buffer{
		return this.message(message, MessageType.Message);
	}
	
	/**
	 * Shows a red message in the middle of the screen.
	 * Player has to click or press a key to continue.
	 * @param message
	 */
	public static showError(message: string): Buffer{
		return this.message(message, MessageType.Error);
	}
	
	/**
	 * Shows a message in chat on the system channel.
	 * @param message
	 */
	public static showSystem(message: string): Buffer{
		return this.message(message, MessageType.System);
	}
	
	private static message(message: string, mode: MessageType): Buffer{
		let packet = createPacket(16 + message.length + 1);
		packet.writeUInt32LE(PacketType.ShowMessage, 8);
		packet.writeUInt8(mode, 12);
		packet.write(message, 16, 'ascii');
		
		return packet;
	}
}
