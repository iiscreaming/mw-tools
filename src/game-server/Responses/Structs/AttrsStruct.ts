import type { Pet } from '../../GameState/Pet/Pet';
import type { Player } from '../../GameState/Player/Player';

export class AttrsStruct{
	public static readonly size: number = 28;
	
	/**
	 * Writes player or pet attributes (level, hp, speed, etc.) to the buffer.
	 * @param buffer
	 * @param ind
	 * @param offset
	 */
	public static write(buffer: Buffer, ind: Player | Pet, offset: number = 0): void{
		let stats = ind.fightData.stats;
		buffer.writeUInt32LE(ind.level.level, offset);
		buffer.writeUInt32LE(stats.totalHp, offset + 4);
		buffer.writeUInt32LE(stats.hp, offset + 8);
		buffer.writeUInt32LE(stats.totalMp, offset + 12);
		buffer.writeUInt32LE(stats.mp, offset + 16);
		buffer.writeUInt32LE(stats.attack, offset + 20);
		buffer.writeUInt32LE(stats.speed, offset + 24);
	}
}
