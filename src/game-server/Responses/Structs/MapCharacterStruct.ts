import type { Npc } from '../../GameState/Npc/Npc';
import { Pet } from '../../GameState/Pet/Pet';
import { Player } from '../../GameState/Player/Player';

export class MapCharacterStruct{
	public static readonly size: number = 84;
	
	/**
	 * Write map character data to the buffer.
	 * @param buffer
	 * @param ind
	 * @param offset
	 */
	public static write(buffer: Buffer, ind: Player | Pet | Npc, offset: number = 0): void{
		buffer.writeUInt32LE(ind.id, offset);
		buffer.write(ind.name, offset + 4, 14, 'ascii');
		buffer.writeUInt32LE(ind.file, offset + 52);
		buffer.writeUInt16LE(ind.mapData.x, offset + 58);
		buffer.writeUInt16LE(ind.mapData.y, offset + 60);
		buffer.writeUInt16LE(ind.mapData.destX, offset + 62);
		buffer.writeUInt16LE(ind.mapData.destY, offset + 64);
		// 66 0 = walk 1 = run ?
		// 67 movement type?
		buffer.writeUInt8(ind.mapData.direction, offset + 68);
		buffer.writeUInt8(ind.mapData.canWalk ? 1 : 0, offset + 69); // TODO 2 or other options?
		buffer.writeInt32LE(-1, offset + 80); // Puts icon in front of body, what for?
		
		if(ind instanceof Player){
			if(ind.titles.title)
				buffer.write(ind.titles.title, offset + 25, 20, 'ascii');
			
			buffer.writeUInt8(ind.class, offset + 56);
			buffer.writeUInt16LE(ind.effect.value, offset + 72);
			buffer.writeUInt16LE(ind.level.reborn, offset + 76);
		}
		else if(ind instanceof Pet){
			// TODO buffer.writeUInt16LE(ind.effect.value, offset + 72);
			buffer.writeUInt16LE(ind.level.reborn, offset + 76);
		}
	}
}
