import type { Pet } from '../../GameState/Pet/Pet';
import { AttrsStruct } from './AttrsStruct';
import { ExpStruct } from './ExpStruct';
import { PetStatsStruct } from './PetStatsStruct';

export class PetStruct{
	public static readonly size: number = 116;
	
	/**
	 * Write pet data to the buffer.
	 * @param buffer
	 * @param pet
	 * @param offset
	 */
	public static write(buffer: Buffer, pet: Pet, offset: number = 0): void{
		buffer.writeUInt32LE(pet.id, offset);
		buffer.write(pet.name, offset + 4, 14, 'ascii');
		buffer.writeUInt32LE(pet.loyalty, offset + 20);
		buffer.writeUInt32LE(pet.intimacy, offset + 24);
		AttrsStruct.write(buffer, pet, offset + 28);
		ExpStruct.write(buffer, pet.level, offset + 56);
		PetStatsStruct.write(buffer, pet, offset + 68);
	}
}