import type { Player } from '../../GameState/Player/Player';

export class PlayerStatsStruct{
	public static readonly size: number = 68;
	
	/**
	 * Writes the player stats to the buffer.
	 * @param buffer
	 * @param player
	 * @param offset
	 */
	public static write(buffer: Buffer, player: Player, offset: number = 0): void{
		let stats = player.fightData.stats;
		buffer.writeUInt32LE(player.unusedStats, offset);
		// 4 sta base
		buffer.writeUInt32LE(stats.sta, offset + 8);
		// 12 sta growth
		// 16 int base
		buffer.writeUInt32LE(stats.int, offset + 20);
		// 24 int growth
		// 28 str base
		buffer.writeUInt32LE(stats.str, offset + 32);
		// 36 str growth
		// 40 agi base
		buffer.writeUInt32LE(stats.agi, offset + 44);
		// 48 agi growth
		buffer.writeUInt32LE(player.file, offset + 52);
		// 56 reborn stats?
		// 60 reborn count?
		// 64 ?
	}
}
