import type { Resist } from '../../GameState/Resist';

export class ResistStruct{
	public static readonly size: number = 60;
	
	/**
	 * Write resist data to the buffer.
	 * @param buffer
	 * @param resist
	 * @param offset
	 */
	public static write(buffer: Buffer, resist: Resist, offset: number = 0): void{
		buffer.writeUInt16LE(resist.defense, offset);
		buffer.writeUInt16LE(resist.hitRate, offset + 2);
		buffer.writeUInt16LE(resist.dodgeRate, offset + 4);
		buffer.writeUInt16LE(resist.drainResist, offset + 6);
		buffer.writeUInt16LE(resist.berserkRate, offset + 8);
		buffer.writeUInt16LE(resist.berserkDamage, offset + 10);
		buffer.writeUInt16LE(resist.criticalRate, offset + 12);
		buffer.writeUInt16LE(resist.criticalDamage, offset + 14);
		buffer.writeUInt16LE(resist.criticalResist, offset + 16);
		buffer.writeUInt16LE(resist.comboRate, offset + 18);
		buffer.writeUInt16LE(resist.comboHit, offset + 20);
		buffer.writeUInt16LE(resist.counterAttackRate, offset + 22);
		buffer.writeUInt16LE(resist.counterAttackDamage, offset + 24);
		buffer.writeUInt16LE(resist.pierce, offset + 26);
		buffer.writeUInt16LE(resist.pierceDamage, offset + 28);
		buffer.writeUInt16LE(resist.magicReflect, offset + 30);
		buffer.writeUInt16LE(resist.magicReflectDamage, offset + 32);
		buffer.writeUInt16LE(resist.meleeReflect, offset + 34);
		buffer.writeUInt16LE(resist.meleeReflectDamage, offset + 36);
		buffer.writeUInt16LE(resist.deathResist, offset + 38);
		buffer.writeUInt16LE(resist.evilResist, offset + 40);
		buffer.writeUInt16LE(resist.flashResist, offset + 42);
		buffer.writeUInt16LE(resist.iceResist, offset + 44);
		buffer.writeUInt16LE(resist.fireResist, offset + 46);
		buffer.writeUInt16LE(resist.meleeResist, offset + 48);
		buffer.writeUInt16LE(resist.poisonResist, offset + 50);
		buffer.writeUInt16LE(resist.chaosResist, offset + 52);
		buffer.writeUInt16LE(resist.stunResist, offset + 54);
		buffer.writeUInt16LE(resist.hypnotizeResist, offset + 56);
		buffer.writeUInt16LE(resist.frailtyResist, offset + 58);
	}
}
