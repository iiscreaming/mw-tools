import type { Player } from '../../GameState/Player/Player';

export class PlayerMiscStruct{
	public static readonly size: number = 24;
	
	/**
	 * Writes the player misc stats to the buffer.
	 * @param buffer
	 * @param player
	 * @param offset
	 */
	public static write(buffer: Buffer, player: Player, offset: number = 0): void{
		buffer.writeUInt32LE(player.misc.reputation, offset);
		buffer.writeUInt32LE(player.misc.deaths, offset + 4);
		buffer.writeUInt32LE(player.misc.pkPoints, offset + 8);
		buffer.writeUInt32LE(player.misc.pkKills, offset + 12);
		buffer.writeUInt32LE(player.misc.warExp, offset + 16);
		// 20 ? guild related
	}
}
