import type { Pet } from '../../GameState/Pet/Pet';

export class PetStatsStruct{
	public static readonly size: number = 48;
	
	/**
	 * Writes the pet stats to the buffer.
	 * @param buffer
	 * @param pet
	 * @param offset
	 */
	public static write(buffer: Buffer, pet: Pet, offset: number = 0): void{
		let stats = pet.fightData.stats;
		buffer.writeUInt32LE(pet.unusedStats, offset);
		// 4 sta base
		buffer.writeUInt32LE(stats.sta, offset + 8);
		// 12 int base
		buffer.writeUInt32LE(stats.int, offset + 16);
		// 20 str base
		buffer.writeUInt32LE(stats.str, offset + 24);
		// 28 agi base
		buffer.writeUInt32LE(stats.agi, offset + 32);
		buffer.writeFloatLE(pet.growthRate, offset + 36);
		buffer.writeUInt16LE(pet.species, offset + 40);
		buffer.writeUInt16LE(pet.file, offset + 42);
		buffer.writeUInt8(pet.level.reborn, offset + 44);
	}
}
