import type { Npc } from '../../GameState/Npc/Npc';
import type { Pet } from '../../GameState/Pet/Pet';
import type { Player } from '../../GameState/Player/Player';

export class MapMoveStruct{
	public static readonly size: number = 12;
	
	/**
	 * Write character id and coordinates to the buffer.
	 * @param buffer
	 * @param ind
	 * @param offset
	 */
	public static write(buffer: Buffer, ind: Player | Pet | Npc, offset: number = 0): void{
		buffer.writeUInt32LE(ind.id, offset);
		buffer.writeUInt16LE(ind.mapData.x, offset + 4);
		buffer.writeUInt16LE(ind.mapData.y, offset + 6);
		// 8 0 = walk 1 = run ?
		buffer.writeUInt8(ind.mapData.direction, offset + 9);
	}
}
