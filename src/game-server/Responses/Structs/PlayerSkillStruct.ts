import type { SkillData } from '../../GameState/SkillData';

export class PlayerSkillStruct{
	public static readonly size: number = 10;
	
	/**
	 * Writes the data of a skill to the buffer.
	 * @param buffer
	 * @param skill
	 * @param offset
	 */
	public static write(buffer: Buffer, skill: SkillData, offset: number = 0): void{
		buffer.writeUInt16LE(skill.id, offset);
		buffer.writeUInt16LE(skill.level, offset + 2);
		buffer.writeUInt16LE(skill.exp, offset + 4);
		// 6/8 ?
	}
}
