import type { Level } from '../../GameState/Level';

export class ExpStruct{
	public static readonly size: number = 12;
	
	/**
	 * Write exp data to the buffer.
	 * @param buffer
	 * @param level
	 * @param offset
	 */
	public static write(buffer: Buffer, level: Level, offset: number = 0): void{
		buffer.writeUInt32LE(level.totalExp, offset);
		buffer.writeUInt32LE(level.currentLvlExp, offset + 4);
		buffer.writeUInt32LE(level.neededExp, offset + 8);
	}
}
