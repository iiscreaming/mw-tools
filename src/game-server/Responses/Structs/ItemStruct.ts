import type { Item } from '../../GameState/Item/Item';

export class ItemStruct{
	public static readonly size: number = 24;
	
	/**
	 * Write item data to the buffer.
	 * @param buffer
	 * @param slot
	 * @param item
	 * @param offset
	 */
	public static write(buffer: Buffer, slot: number, item: Item | null, offset: number = 0): void{
		buffer.writeUInt32LE(slot, offset);
		
		if(item !== null){
			buffer.writeInt32LE(1, offset + 4); // id, not actually used?
			buffer.writeUInt8(item.count, offset + 8);
			buffer.writeUInt16LE(item.file, offset + 12);
			buffer.writeUInt8(item.locked ? 1 : 0, offset + 20);
		}
	}
	
	/**
	 * Writes data of multiple items to the buffer.
	 * @param buffer
	 * @param items
	 * @param offset
	 */
	public static writeList(buffer: Buffer, items: Iterable<[slot: number, item: Item | null]>, offset: number = 0): void{
		for(let [slot, item] of items){
			this.write(buffer, slot, item, offset);
			offset += this.size;
		}
	}
}
