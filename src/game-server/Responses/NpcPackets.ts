import { Macro } from '../Enums/Macro';
import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';

export class NpcPackets{
	/**
	 * An NPC dialog without options, closed by clicking anywhere in it.
	 * @param message
	 */
	public static dialogClosable(message: string): Buffer{
		let packet = createPacket(16 + message.length + 1);
		packet.writeUInt16BE(message.length, 6);
		packet.writeUInt32LE(PacketType.SendMacro, 8);
		packet.writeUInt8(Macro.NpcMessageClosable, 12);
		packet.write(message, 16);
		return packet;
	}
	
	/**
	 * An NPC dialog where the player is required to pick one of the options.
	 * @param message A message, followed by byte 0, followed by options seperated with &-characters.
	 */
	public static dialogWithOptions(message: string): Buffer{
		let packet = createPacket(16 + message.length + 1);
		packet.writeUInt16BE(message.length, 6);
		packet.writeUInt32LE(PacketType.SendMacro, 8);
		packet.writeUInt8(Macro.NpcMessageOptions, 12);
		packet.write(message, 16);
		return packet;
	}
}
