import type { FightActionResult } from '../../GameState/Fight/FightActionResultTypes';
import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

export function getFightActionResult(result: FightActionResult): Buffer{
	let length = 40;
	
	if(result.status)
		length += result.status.length * 28;
	
	if(result.magic)
		length += result.magic.length * 12;
	
	if(result.data)
		length += result.data.length;
	
	let packet = createPacket(length);
	packet.writeUInt16LE(length - 12, 6);
	packet.writeUInt32LE(PacketType.FightActionResult, 8);
	
	packet.writeUInt16LE(result.type, 16);
	//18 ?
	packet.writeUInt32LE(result.source, 20);
	packet.writeUInt32LE(result.target, 24);
	packet.writeUInt32LE(result.detail ?? 0, 28);
	
	packet.writeUInt8(result.status?.length ?? 0, 32);
	packet.writeUInt8(result.magic?.length ?? 0, 33);
	//34?
	packet.writeUInt32LE(result.data?.length ?? 0, 36);
	
	let offset = 40;
	
	if(result.status){
		for(let status of result.status){
			packet.writeUInt32LE(status.id, offset);
			packet.writeUInt32LE(status.totalHp, offset + 4);
			packet.writeUInt32LE(status.hp, offset + 8);
			packet.writeUInt32LE(status.totalMp, offset + 12);
			packet.writeUInt32LE(status.mp, offset + 16);
			packet.writeUInt32LE(status.speed, offset + 20);
			packet.writeUInt32LE(status.effect, offset + 24);
			
			offset += 28;
		}
	}
	
	if(result.magic){
		for(let skill of result.magic){
			packet.writeUInt32LE(skill.id, offset);
			packet.writeUInt32LE(skill.damage, offset + 4);
			packet.writeUInt32LE(skill.repel, offset + 8);
			
			offset += 12;
		}
	}
	
	if(result.data)
		result.data.copy(packet, offset);
	
	return packet;
}
