import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

let packet = createPacket(16);
packet.writeUInt32LE(PacketType.FightTurnPause, 8);

export const fightTurnPausePacket = packet;
