import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

let packet = createPacket(16);
packet.writeUInt32LE(PacketType.FightEnd, 8);

export const fightEndPacket = packet;
