import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

/**
 * Tells the client to start the next turn, and whose turn it is.
 * @param id
 */
export function getFightTurnContinue(id: number): Buffer{
	let packet = createPacket(16);
	packet.writeUInt32LE(PacketType.FightTurnContinue, 8);
	packet.writeUInt32LE(id, 12);
	
	return packet;
}
