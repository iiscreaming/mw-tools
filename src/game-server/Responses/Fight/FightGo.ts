import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';

let packet = createPacket(16);
packet.writeUInt32LE(PacketType.FightGo, 8);

export const fightGoPacket = packet;
