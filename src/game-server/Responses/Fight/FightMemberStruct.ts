import type { FightMember } from '../../GameState/Fight/FightMember';
import { Pet } from '../../GameState/Pet/Pet';
import { Player } from '../../GameState/Player/Player';

export class FightMemberStruct{
	public static readonly size: number = 68;
	
	/**
	 * Writes the data of a character in battle to the buffer.
	 * @param buffer
	 * @param member
	 * @param offset
	 */
	public static write(buffer: Buffer, member: FightMember, offset: number = 0): void{
		buffer.writeUInt8(member.location, offset);
		buffer.writeUInt8(member.active ? 1 : 0, offset + 4);
		buffer.writeUInt32LE(member.base.id, offset + 8);
		buffer.write(member.base.name, offset + 12, 14, 'ascii');
		
		buffer.writeUInt32LE(member.base.fightData.stats.totalHp, offset + 36);
		buffer.writeUInt32LE(member.base.fightData.stats.hp, offset + 40);
		buffer.writeUInt32LE(member.base.fightData.stats.totalMp, offset + 44);
		buffer.writeUInt32LE(member.base.fightData.stats.mp, offset + 48);
		buffer.writeUInt16LE(member.base.fightData.stats.speed, offset + 52);//uint8 charge
		
		buffer.writeUInt16LE(member.base.file, offset + 54);
		buffer.writeUInt8(member.type, offset + 56);
		buffer.writeUInt32LE(member.effect.value, offset + 60);
		
		if(member.base instanceof Player || member.base instanceof Pet)
			buffer.writeUInt8(member.base.level.reborn, offset + 64);
	}
}
