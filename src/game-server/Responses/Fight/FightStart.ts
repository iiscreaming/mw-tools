import type { Fight } from '../../GameState/Fight/Fight';
import { createPacket } from '../../PacketBuilder';
import { PacketType } from '../../PacketType';
import { FightMemberStruct } from './FightMemberStruct';

/**
 * Tells the client to open a fight.
 * @param fight
 */
export function getFightStart(fight: Fight): Buffer{
	let count = fight.members.size;
	let packet = createPacket(16 + count * 68);
	packet.writeUInt32LE(PacketType.FightStart, 8);
	packet.writeUInt32LE(count, 12);
	
	let offset = 16;
	
	for(let member of fight.members.values()){
		FightMemberStruct.write(packet, member, offset);
		offset += 68;
	}
	
	return packet;
}
