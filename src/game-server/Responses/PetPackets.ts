import type { Pet } from '../GameState/Pet/Pet';
import type { Player } from '../GameState/Player/Player';
import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';
import { AttrsStruct } from './Structs/AttrsStruct';
import { ExpStruct } from './Structs/ExpStruct';
import { PetStatsStruct } from './Structs/PetStatsStruct';
import { PetStruct } from './Structs/PetStruct';
import { ResistStruct } from './Structs/ResistStruct';

export class PetPackets{
	/**
	 * Contains the players pets.
	 * @param pets
	 */
	public static list(pets: Pet[]): Buffer{
		let packet = createPacket(16 + pets.length * PetStruct.size);
		packet.writeUInt32LE(PacketType.PetList, 8);
		packet.writeUInt32LE(pets.length, 12);
		
		for(let i = 0; i < pets.length; ++i)
			PetStruct.write(packet, pets[i], 16 + i * PetStruct.size);
		
		return packet;
	}
	
	/**
	 * The result of the player changing the pet's name.
	 * @param pet
	 * @param success
	 */
	public static rename(pet: Pet, success: boolean = true): Buffer{
		let packet = createPacket(20);
		packet.writeUInt32LE(PacketType.PetRename, 8);
		packet.writeInt32LE(success ? 0 : -1, 12);
		packet.writeUInt32LE(pet.id, 16);
		return packet;
	}
	
	/**
	 * The result of using the pet's stat points.
	 * @param pet
	 */
	public static useStats(pet: Pet): Buffer{
		let packet = createPacket(16 + AttrsStruct.size + ExpStruct.size);
		packet.writeUInt16LE(AttrsStruct.size + ExpStruct.size, 6);
		packet.writeUInt32LE(PacketType.PetUseStats, 8);
		packet.writeUInt32LE(pet.id, 12);
		AttrsStruct.write(packet, pet, 16);
		ExpStruct.write(packet, pet.level, 16 + AttrsStruct.size);
		return packet;
	}
	
	/**
	 * Used when pet level changes, updates stats, exp.
	 * @param pet
	 */
	public static level(pet: Pet): Buffer{
		let packet = createPacket(104);
		packet.writeUInt16LE(88, 6);
		packet.writeUInt32LE(PacketType.PetLevel, 8);
		packet.writeUInt32LE(pet.id, 12);
		PetStatsStruct.write(packet, pet, 16);
		AttrsStruct.write(packet, pet, 64);
		ExpStruct.write(packet, pet.level, 92);
		return packet;
	}
	
	/**
	 * Used when pet exp changes.
	 * @param pet
	 */
	public static experience(pet: Pet): Buffer{
		let packet = createPacket(16 + ExpStruct.size);
		packet.writeUInt16LE(ExpStruct.size, 6);
		packet.writeUInt32LE(PacketType.PetExperience, 8);
		packet.writeUInt32LE(pet.id, 12);
		ExpStruct.write(packet, pet.level, 16);
		return packet;
	}
	
	/**
	 * Set a pet on follow.
	 * @param pet
	 * @param owner
	 */
	public static follow(pet: Pet, owner: Player): Buffer{
		let packet = createPacket(44);
		packet.writeUInt32LE(PacketType.PetFollow, 8);
		packet.writeUInt32LE(owner.id, 12);
		packet.writeUInt32LE(pet.id, 16);
		packet.writeUInt32LE(owner.id, 20);
		packet.writeUInt16LE(pet.file, 24);
		packet.writeUInt8(pet.level.reborn, 26);
		packet.write(pet.name, 28, 14, 'ascii');
		return packet;
	}
	
	/**
	 * Unset a pet from following.
	 * @param pet
	 */
	public static unfollow(pet: Pet): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.PetUnfollow, 8);
		packet.writeUInt32LE(pet.id, 12);
		return packet;
	}
	
	/**
	 * Set a pet as active.
	 * @param pet
	 */
	public static battle(pet: Pet): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.PetBattle, 8);
		packet.writeUInt32LE(pet.id, 12);
		return packet;
	}
	
	/**
	 * Set a pet as inactive.
	 * @param pet
	 */
	public static unbattle(pet: Pet): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.PetUnbattle, 8);
		packet.writeUInt32LE(pet.id, 12);
		return packet;
	}
	
	/**
	 * Add a pet to the player.
	 * @param pet
	 */
	public static add(pet: Pet): Buffer{
		let packet = createPacket(16 + PetStruct.size + ResistStruct.size);
		packet.writeUInt16LE(PetStruct.size + ResistStruct.size, 6);
		packet.writeUInt32LE(PacketType.PetAdd, 8);
		packet.writeUInt32LE(1, 12);
		PetStruct.write(packet, pet, 16);
		ResistStruct.write(packet, pet.fightData.resist, 16 + PetStruct.size);
		return packet;
	}
	
	/**
	 * Remove a pet from the player.
	 * @param id
	 */
	public static remove(id: number): Buffer{
		let packet = createPacket(20);
		packet.writeUInt32LE(PacketType.PetRemove, 8);
		packet.writeUInt32LE(id, 16);
		return packet;
	}
	
	/**
	 * Updates the pet's resist data.
	 * @param pets
	 */
	public static resist(pet: Pet): Buffer{
		let packet = createPacket(16 + ResistStruct.size);
		packet.writeUInt32LE(PacketType.PetResist, 8);
		packet.writeUInt32LE(pet.id, 12);
		ResistStruct.write(packet, pet.fightData.resist, 16);
		return packet;
	}
	
	/**
	 * Update the pet's stats.
	 * @param pet
	 */
	public static stats(pet: Pet): Buffer{
		let packet = createPacket(16 + PetStatsStruct.size);
		packet.writeUInt16LE(PetStatsStruct.size, 6);
		packet.writeUInt32LE(PacketType.PetStats, 8);
		PetStatsStruct.write(packet, pet, 16);
		return packet;
	}
	
	/**
	 * Update the pet's attributes.
	 * @param pet
	 */
	public static attributes(pet: Pet): Buffer{
		let packet = createPacket(16 + AttrsStruct.size + ExpStruct.size);
		packet.writeUInt16LE(AttrsStruct.size + ExpStruct.size, 6);
		packet.writeUInt32LE(PacketType.PetAttributes, 8);
		AttrsStruct.write(packet, pet, 16);
		ExpStruct.write(packet, pet.level, 16 + AttrsStruct.size);
		return packet;
	}
	
	/**
	 * Updates the active pet's hp, will show an animation.
	 * @param pet
	 */
	public static healHp(pet: Pet): Buffer{
		let packet = createPacket(20);
		packet.writeUInt32LE(PacketType.PetHealHp, 8);
		packet.writeUInt32LE(pet.fightData.stats.hp, 12);
		packet.writeUInt32LE(pet.id, 16);
		return packet;
	}
	
	/**
	 * Updates the active pet's mp, will show an animation.
	 * @param pet
	 */
	public static healMp(pet: Pet): Buffer{
		let packet = createPacket(20);
		packet.writeUInt32LE(PacketType.PetHealMp, 8);
		packet.writeUInt32LE(pet.fightData.stats.mp, 12);
		packet.writeUInt32LE(pet.id, 16);
		return packet;
	}
}
