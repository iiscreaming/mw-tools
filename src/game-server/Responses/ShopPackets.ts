import { Macro } from '../Enums/Macro';
import type { Item } from '../GameState/Item/Item';
import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';

export class ShopPackets{
	/**
	 * Opens an npc shop.
	 * @param items
	 */
	public static npcVendor(items: Item[]): Buffer[]{
		let packet1 = createPacket(16);
		packet1.writeUInt32LE(PacketType.SendMacro, 8);
		packet1.writeUInt8(Macro.ViewShop, 12);
		
		let packet2 = createPacket(16 + items.length * 20);
		packet2.writeUInt32LE(PacketType.ShopItemList, 8);
		packet2.writeUInt32LE(items.length, 12);
		
		let offset = 16;
		for(let item of items){
			packet2.writeInt32LE(1, offset); // id, not actually used?
			packet2.writeUInt8(0, offset + 4); // count? also not used?
			packet2.writeUInt16LE(item.file, offset + 8);
			packet2.writeUInt16LE(item.price, offset + 12);
			offset += 20;
		}
		
		return [packet1, packet2];
	}
}
