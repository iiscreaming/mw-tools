import type { Player } from '../GameState/Player/Player';
import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';
import { AttrsStruct } from './Structs/AttrsStruct';
import { ExpStruct } from './Structs/ExpStruct';
import { PlayerMiscStruct } from './Structs/PlayerMiscStruct';
import { PlayerSkillStruct } from './Structs/PlayerSkillStruct';
import { PlayerStatsStruct } from './Structs/PlayerStatsStruct';
import { ResistStruct } from './Structs/ResistStruct';

export class PlayerPackets{
	/**
	 * Contains most information for the stats screen.
	 * @param player
	 */
	public static information(player: Player): Buffer{
		let packet = createPacket(168);
		packet.writeUInt16LE(152, 6);
		packet.writeUInt32LE(PacketType.PlayerInformation, 8);
		
		if(player.guild)
			packet.write(player.guild.name, 16, 14, 'ascii');
		
		AttrsStruct.write(packet, player, 32);
		ExpStruct.write(packet, player.level, 60);
		packet.writeUInt32LE(player.file, 72);
		PlayerStatsStruct.write(packet, player, 76);
		PlayerMiscStruct.write(packet, player, 144);
		
		return packet;
	}
	
	/**
	 * Result of using stats, updates attributes.
	 * @param player
	 */
	public static useStats(player: Player): Buffer{
		let packet = createPacket(60);
		packet.writeUInt16LE(44, 6);
		packet.writeUInt32LE(PacketType.PlayerUseStats, 8);
		packet.writeUInt32LE(1, 12); // 1 = success, otherwise error
		
		AttrsStruct.write(packet, player, 16);
		ExpStruct.write(packet, player.level, 44);
		packet.writeUInt32LE(player.file, 56);
		
		return packet;
	}
	
	/**
	 * Used when player level changes, updates stats, exp.
	 * @param player
	 */
	public static level(player: Player): Buffer{
		let packet = createPacket(128);
		packet.writeUInt16LE(112, 6);
		packet.writeUInt32LE(PacketType.PlayerLevel, 8);
		
		PlayerStatsStruct.write(packet, player, 16);
		AttrsStruct.write(packet, player, 84);
		ExpStruct.write(packet, player.level, 112);
		packet.writeUInt32LE(player.file, 124);
		
		return packet;
	}
	
	/**
	 * Used when player exp changes.
	 * @param player
	 */
	public static experience(player: Player): Buffer{
		let packet = createPacket(28);
		packet.writeUInt16LE(12, 6);
		packet.writeUInt32LE(PacketType.PlayerExperience, 8);
		ExpStruct.write(packet, player.level, 16);
		
		return packet;
	}
	
	/**
	 * Sends the player's resist data.
	 * @param player
	 */
	public static resist(player: Player): Buffer{
		let packet = createPacket(76);
		packet.writeUInt16LE(60, 6);
		packet.writeUInt32LE(PacketType.PlayerResist, 8);
		ResistStruct.write(packet, player.fightData.resist, 16);
		
		return packet;
	}
	
	/**
	 * Updates player misc stats (reputation, etc.).
	 * @param player
	 */
	public static misc(player: Player): Buffer{
		let packet = createPacket(40);
		packet.writeUInt32LE(24, 6);
		packet.writeUInt32LE(PacketType.PlayerMisc, 8);
		PlayerMiscStruct.write(packet, player, 16);
		
		return packet;
	}
	
	/**
	 * Updates the player skill data.
	 * @param player
	 */
	public static skills(player: Player): Buffer{
		let skills = player.fightData.skills;
		let packet = createPacket(16 + skills.length * 10);
		packet.writeUInt32LE(PacketType.PlayerSkills, 8);
		packet.writeUInt32LE(skills.length, 12);
		
		for(let i = 0; i < skills.length; ++i)
			PlayerSkillStruct.write(packet, skills[i], 16 + i * 10);
		
		return packet;
	}
	
	/**
	 * Updates the player's hp, will show an animation.
	 * @param currentHp
	 */
	public static healHp(currentHp: number): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.PlayerHealHp, 8);
		packet.writeUInt32LE(currentHp, 12);
		return packet;
	}
	
	/**
	 * Updates the player's mp, will show an animation.
	 * @param currentMp
	 */
	public static healMp(currentMp: number): Buffer{
		let packet = createPacket(16);
		packet.writeUInt32LE(PacketType.PlayerHealMp, 8);
		packet.writeUInt32LE(currentMp, 12);
		return packet;
	}
}
