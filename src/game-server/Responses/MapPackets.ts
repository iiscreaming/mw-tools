import type { Player } from '../GameState/Player/Player';
import { createPacket } from '../PacketBuilder';
import { PacketType } from '../PacketType';
import { MapCharacterStruct } from './Structs/MapCharacterStruct';
import { MapMoveStruct } from './Structs/MapMoveStruct';

/* TODO:
How are walkDestinations and updateCoordinates different?
Should they include npcs?
Only nearby characters?
Any other parameters?
*/

export class MapPackets{
	/**
	 * Opens the map.
	 * Client needs to have recieved packet LocationData before this.
	 * Only used on the first map, SceneChange is used afterwards.
	 */
	public static sceneEnter: Buffer = (() => {
		let packet = createPacket(12);
		packet.writeUInt32LE(PacketType.SceneEnter, 8);
		return packet;
	})();
	
	/**
	 * Opens a new map.
	 */
	public static sceneChange: Buffer = (() => {
		let packet = createPacket(12);
		packet.writeUInt32LE(PacketType.SceneChange, 8);
		return packet;
	})();
	
	/**
	 * Update coordinates of people.
	 * @param player
	 */
	public static walkDestinations(player: Player): Buffer{
		let map = player.mapData.map;
		
		if(!map)
			throw Error('MapPackets.walkDestinations: no map.');
		
		let packet = createPacket(16 + 12 * map.players.length);
		packet.writeUInt32LE(PacketType.WalkDestinations, 8);
		packet.writeUInt32LE(map.players.length, 12);
		
		for(let i = 0; i < map.players.length; ++i)
			MapMoveStruct.write(packet, map.players[i], 16 + i * 12);
		
		return packet;
	}
	
	/**
	 * Update coordinates of people.
	 * @param player 
	 */
	public static updateCoordinates(player: Player): Buffer{
		let map = player.mapData.map;
		
		if(!map)
			throw Error('MapPackets.updateCoordinates: no map.');
		
		let inds = [...map.players, ...map.npcs];
		let packet = createPacket(16 + 12 * inds.length);
		packet.writeUInt32LE(PacketType.UpdateCoordinates, 8);
		packet.writeUInt32LE(inds.length, 12);
		
		for(let i = 0; i < inds.length; ++i)
			MapMoveStruct.write(packet, inds[i], 16 + i * 12);
		
		return packet;
	}	
	
	/**
	 * Loads all characters on the map.
	 * @param player
	 */
	public static mapPopulation(player: Player): Buffer{
		let map = player.mapData.map;
		
		if(!map)
			throw Error('MapPackets.mapPopulation: no map.');
		
		let inds = [...map.players, ...map.npcs];
		inds.splice(inds.indexOf(player), 1);
		
		let packet = createPacket(16 + 84 * inds.length);
		packet.writeUInt32LE(PacketType.MapPopulation, 8);
		packet.writeUInt32LE(inds.length, 12);
		
		for(let i = 0; i < inds.length; ++i)
			MapCharacterStruct.write(packet, inds[i], 16 + 84 * i);
		
		return packet;
	}
	
	/**
	 * Load a new location.
	 * Loads the map and player data.
	 * @param player
	 */
	public static locationData(player: Player): Buffer{
		let map = player.mapData.map;
		
		if(!map)
			throw Error('MapPackets.locationData: no map.');
		
		let packet = createPacket(188);
		packet.writeUInt16LE(172, 6);
		packet.writeUInt32LE(PacketType.LocationData, 8);
		
		packet.write(map.name, 16, 12, 'ascii');
		packet.writeUInt32LE(map.file, 32);
		packet.writeUInt32LE(map.musicFile, 36);
		packet.writeUInt32LE(map.minimapFile, 40);
		packet.writeUInt32LE(map.fightMusicFile, 44);
		packet.writeUInt32LE(map.fightBackgroundFile, 48);
		// 52 boss fight music
		// 56 boss fight background
		// 60 fighting enabled
		// 64 reputation blocks pk
		// 68 pk enabled
		packet.writeUInt32LE(map.minimapEnabled ? 1 : 0, 72);
		packet.writeUInt32LE(map.weather, 76);
		// 80 - 104 weather effects on gold, mobs, etc
		MapCharacterStruct.write(packet, player, 104);
		
		return packet;
	}
}
