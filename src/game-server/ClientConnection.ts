import type { Socket } from 'net';
import { getPacketType, readPackets } from './PacketReader';
import type { GameServer } from './GameServer';
import type { User } from './GameState/User';
import type { Player } from './GameState/Player/Player';
import type { Game } from './GameState/Game';
import { PacketType } from './PacketType';

export type ClientConnectionWithPlayer = ClientConnection & {player: Player};

/**
 * Manages a connection between server and client.
 */
export class ClientConnection{
	public game: Game;
	
	public user: User | null = null;
	
	public player: Player | null = null;
	
	public constructor(
		private socket: Socket,
		private server: GameServer,
	){
		this.game = server.game;
		
		socket.setDefaultEncoding('binary');
		
		socket.on('data', data => this.onData(data));
		socket.on('close', hadError => this.onClose(hadError));
		socket.on('end', () => this.onEnd());
		socket.on('error', err => this.onError(err));
		socket.on('timeout', () => this.onTimeout());
	}
	
	/**
	 * Sends the data to the client.
	 * @param buffer
	 */
	public write(...buffers: (Buffer | null)[]): void{
		for(let buffer of buffers){
			if(!buffer)
				return;
			
			let type = getPacketType(buffer);
			console.log('Writing', PacketType[type], buffer);
			this.socket.write(buffer);
		}
	}
	
	/**
	 * Close the connection to the client.
	 */
	public closeConnection(): void{
		this.socket.end();
	}
	
	/**
	 * Handles the data received from the client.
	 * @param data
	 */
	private onData(data: Buffer): void{
		let packets = readPackets(data);
		
		for(let packet of packets){
			this.handlePacket(packet);
		}
	}
	
	/**
	 * Forwards a packet to the appropriate packet handler.
	 * @param packet
	 */
	private handlePacket(packet: Buffer): void{
		let type = getPacketType(packet);
		let handler = this.server.packetHandlers.getPacketHandler(type);
		
		if(handler === null){
			console.log(packet);
			console.error('Unhandled packet type ' + type.toString(16) + ' ' + PacketType[type]);
			
			return;
		}
		
		handler.handlePacket(packet, this);
	}
	
	/**
	 * Called when the connection has been closed.
	 * @param hadError
	 */
	private onClose(hadError: boolean): void{
		let index = this.server.connections.indexOf(this);
		this.server.connections.splice(index, 1);
		
		if(this.player)
			this.player.client = null;
		
		console.log('Socket closed' + (hadError ? ' with an error' : ''));
	}
	
	/**
	 * Called when the connection gets ended by the client.
	 */
	private onEnd(): void{
		console.log('Socket end');
	}
	
	/**
	 * Called when a connection error happens.
	 * @param err
	 */
	private onError(err: Error): void{
		console.log('Socket error', err);
	}
	
	/**
	 * Called when the connection times out.
	 */
	private onTimeout(): void{
		console.log('Socket timeout');
	}
}
