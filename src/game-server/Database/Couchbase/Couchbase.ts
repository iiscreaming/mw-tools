// Re-export couchbase so it uses the fixed types in ./types.d.ts
// eslint-disable-next-line @typescript-eslint/no-require-imports, @typescript-eslint/no-var-requires
export = require('couchbase') as typeof couchbase;
