import type { IBucketManager } from '../Interfaces/IBucketManager';

export class CBBucketManager implements IBucketManager{
	private buckets: couchbase.BucketManager;
	
	public constructor(cluster: couchbase.Cluster){
		this.buckets = cluster.buckets();
	}
	
	public async getAllBuckets(): Promise<couchbase.BucketSettings[]>{
		return this.buckets.getAllBuckets();
	}
	
	public async createBucket(settings: couchbase.CreateBucketSettings): Promise<void>{
		await this.buckets.createBucket(settings);
	}
}
