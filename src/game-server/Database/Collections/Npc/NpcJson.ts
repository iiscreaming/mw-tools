import type { Direction } from '../../../Enums/Direction';
import type { GameAction } from '../../../GameActions/GameActionTypes';

export type NpcJson = {
	id: number,
	name: string,
	file: number,
	map: number,
	x: number,
	y: number,
	direction: Direction,
	action?: GameAction,
};
