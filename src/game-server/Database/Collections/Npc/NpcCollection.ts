import { GameActionParser } from '../../../GameActions/GameActionParser';
import { Npc } from '../../../GameState/Npc/Npc';
import { BaseCollection } from '../BaseCollection';
import type { NpcJson } from './NpcJson';

export class NpcCollection extends BaseCollection<Npc, NpcJson>{
	private static instance: NpcCollection | null = null;
	
	protected constructor(){
		super('Npc');
	}
	
	public static getInstance(): NpcCollection{
		if(this.instance === null)
			this.instance = new NpcCollection();
		
		return this.instance;
	}
	
	public getKey(obj: Npc): string{
		return obj.id.toString();
	}
	
	protected toJson(): NpcJson{
		// Would this ever be needed?
		throw Error('Saving NPCs is not supported.');
	}
	
	protected fromJson(json: NpcJson): Npc{
		let npc = new Npc();
		
		npc.id = json.id;
		npc.name = json.name;
		npc.file = json.file;
		npc.defaultMap = json.map;
		npc.mapData.x = npc.mapData.destX = json.x;
		npc.mapData.y = npc.mapData.destY = json.y;
		npc.mapData.direction = json.direction;
		npc.action = GameActionParser.parse(json.action);
		
		return npc;
	}
}
