export type MapData = {
	id: number,
	name: string,
	file: number,
	musicFile: number,
	minimapFile: number,
	fightMusicFile: number,
	fightBackgroundFile: number,
};
