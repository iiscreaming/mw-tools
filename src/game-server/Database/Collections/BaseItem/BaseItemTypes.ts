import type { EquipmentSlot } from '../../../Enums/EquipmentSlot';
import type { GameActionExecutable } from '../../../GameActions/GameActionExecutable';
import type { GameAction } from '../../../GameActions/GameActionTypes';
import type { ItemType } from '../../../GameState/Item/ItemType';

export type BaseItemJson = {
	id: number,
	file: number,
	stackLimit: number,
	name: string,
	description: string,
	type: ItemType,
	equipmentSlot?: EquipmentSlot | null,
	action?: GameAction | null,
};

export type BaseItem = {
	id: number,
	file: number,
	stackLimit: number,
	name: string,
	description: string,
	type: ItemType,
	equipmentSlot: EquipmentSlot | null,
	action: GameActionExecutable | null,
};
