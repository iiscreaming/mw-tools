import { Config } from '../Config/Config';
import { CBCluster } from './Couchbase/CBCluster';
import type { IBucket } from './Interfaces/IBucket';
import type { ICluster } from './Interfaces/ICluster';
import { FileCluster } from './File/FileCluster';

export class Database{
	private static instance: Database | null = null;
	public readonly cluster: ICluster;
	private bucket: IBucket | null = null;
	
	protected constructor(){
		let config = Config.get();
		
		if(config.databaseType === 'file')
			this.cluster = new FileCluster(module.path + '/../file-storage');
		else if(config.databaseType === 'couchbase')
			this.cluster = new CBCluster(config.couchbase.host, config.couchbase.username, config.couchbase.password);
		else
			throw Error('Invalid config.databaseType');
	}
	
	/**
	 * Get an instance of this class.
	 */
	public static get(): Database{
		if(this.instance === null)
			this.instance = new Database();
		
		return this.instance;
	}
	
	/**
	 * Closes the cluster and unsets the instance.
	 */
	public static async reset(): Promise<void>{
		if(this.instance !== null){
			await this.instance.cluster.close();
			this.instance = null;
		}
	}
	
	/**
	 * Get the main bucket for the server.
	 */
	public async mainBucket(): Promise<IBucket>{
		if(this.bucket === null)
			this.bucket = await this.cluster.bucket('MythWarServer');
		
		return this.bucket;
	}
}
