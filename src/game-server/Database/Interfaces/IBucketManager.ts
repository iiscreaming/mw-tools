export interface IBucketManager{
	/**
	 * Get list of buckets.
	 */
	getAllBuckets(): Promise<couchbase.BucketSettings[]>;
	
	/**
	 * Create a new bucket.
	 * @param settings
	 */
	createBucket(settings: couchbase.CreateBucketSettings): Promise<void>;
}
