import { readFileSync } from 'fs';

type IConfig = {
	world: {
		ip: string,
		port: number,
	},
	chat: {
		ip: string,
		port: number,
	},
	databaseType: 'file' | 'couchbase',
	couchbase: {
		host: string,
		username: string,
		password: string,
	},
	security: {
		globalSalt: string,
	},
};

export class Config implements IConfig{
	private static instance: Config | null = null;
	private static path: string = module.path + '/../server.config.json';
	public world!: IConfig['world'];
	public chat!: IConfig['chat'];
	public databaseType: IConfig['databaseType'] = 'file';
	public couchbase!: IConfig['couchbase'];
	public security!: IConfig['security'];
	
	protected constructor(){}
	
	/**
	 * Return instance of this class.
	 */
	public static get(): Config{
		if(this.instance === null)
			this.instance = this.load();
		
		return this.instance;
	}
	
	/**
	 * Load the config file.
	 */
	private static load(): Config{
		let config = new Config();
		config.load(this.path);
		
		return config;
	}
	
	/**
	 * Put config values in this class.
	 * @param file
	 */
	private load(file: string): void{
		let contents = this.loadFile(file);
		
		try{
			let obj = JSON.parse(contents);
			this.world = obj.world;
			this.chat = obj.chat;
			this.databaseType = obj.databaseType;
			this.couchbase = obj.couchbase;
			this.security = obj.security;
		}
		catch(e: unknown){
			throw Error('Could not parse config file.');
		}
	}
	
	/**
	 * Read the file contents.
	 * @param file
	 */
	private loadFile(file: string): string{
		try{
			return readFileSync(file, {encoding: 'utf8'});
		}
		catch(e: unknown){
			throw Error('Could not read config file.');
		}
	}
}
