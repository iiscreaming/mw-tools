export type OptionalParams<T> = T extends undefined ? [] : [T];
