/**
 * Returns the string up to the first zero byte.
 * @param str
 */
export function endAtZero(str: string): string{
	let index = str.indexOf('\0');
	return index === -1 ? str : str.substr(0, index);
}

/**
 * Check if a string is a valid name.
 * Allows letters, numbers, `-` and `_`.
 * @param str
 */
export function isValidName(str: string): boolean{
	let regex = /^[a-zA-Z0-9\-_]{1,14}$/;
	return regex.test(str);
}
