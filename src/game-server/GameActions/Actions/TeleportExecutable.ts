import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionSingle, GameActionTeleport } from '../GameActionTypes';

/**
 * Executes a teleport.
 */
export class TeleportExecutable extends GameActionExecutable{
	protected constructor(
		protected action: GameActionSingle,
		protected targetNpcId: number,
	){super(action);}
	
	public static parse(action: GameActionTeleport): GameActionExecutable{
		return new this(action, action.targetNpcId);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		let npc = client.game.npcs.get(this.targetNpcId);
		let map = npc?.mapData.map;
		
		if(!npc || !map)
			return;
		
		// TODO: reduce gold
		map.addPlayer(client.player);
		let x = npc.mapData.x - 16;
		let y = npc.mapData.y - 16;
		client.player.mapData.setNearPoint(x, y, 5, 10);
		client.player.enterMap();
	}
}
