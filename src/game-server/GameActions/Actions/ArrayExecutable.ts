import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameActionExecutable } from '../GameActionExecutable';
import { GameActionParser } from '../GameActionParser';
import type { GameActionArray, GameActionSingle } from '../GameActionTypes';

/**
 * Executes multiple executables.
 */
export class ArrayExecutable extends GameActionExecutable{
	protected constructor(
		protected action: GameActionSingle,
		protected actions: GameActionExecutable[],
	){super(action);}
	
	public static parse(action: GameActionArray): GameActionExecutable{
		let actions = action.actions.map(act => GameActionParser.parse(act));
		return new this(action, actions);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		this.actions.forEach(act => act.execute(client));
	}
}
