import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { PetPackets } from '../../Responses/PetPackets';
import { PlayerPackets } from '../../Responses/PlayerPackets';
import { Random } from '../../Utils/Random';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionExp } from '../GameActionTypes';

/**
 * Give exp to the player or pet.
 */
export class AddExpExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionExp,
	){super(action);}
	
	public static parse(action: GameActionExp): GameActionExecutable{
		return new this(action);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		let exp = Random.fromJson(this.action.exp);
		
		if(this.action.pet)
			this.runPet(client, exp);
		else
			this.runPlayer(client, exp);
	}
	
	private runPlayer(client: ClientConnectionWithPlayer, exp: number): void{
		let levels = client.player.level.addExp(exp);
		
		if(levels === 0)
			client.write(PlayerPackets.experience(client.player));
		else
			client.write(PlayerPackets.level(client.player));
	}
	
	private runPet(client: ClientConnectionWithPlayer, exp: number): void{
		let pet = client.player.activePet;
		
		if(!pet)
			return;
		
		let levels = pet.level.addExp(exp);
		
		if(levels === 0)
			client.write(PetPackets.experience(pet));
		else
			client.write(PetPackets.level(pet));
	}
}
