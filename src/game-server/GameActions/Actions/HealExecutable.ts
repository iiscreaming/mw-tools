import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import type { Stats } from '../../GameState/Stats';
import { PetPackets } from '../../Responses/PetPackets';
import { PlayerPackets } from '../../Responses/PlayerPackets';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionHeal } from '../GameActionTypes';

/**
 * Heals the player or pet.
 */
export class HealExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionHeal,
	){super(action);}
	
	public static parse(action: GameActionHeal): GameActionExecutable{
		return new this(action);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		if(this.action.pet)
			this.runPet(client);
		else
			this.runPlayer(client);
	}
	
	private runPlayer(client: ClientConnectionWithPlayer): void{
		let stats = client.player.fightData.stats;
		this.doHeal(stats);
		
		if(this.action.hp)
			client.write(PlayerPackets.healHp(stats.hp));
		if(this.action.mp)
			client.write(PlayerPackets.healMp(stats.mp));
	}
	
	private runPet(client: ClientConnectionWithPlayer): void{
		let pet = client.player.activePet;
		
		if(!pet)
			return;
		
		this.doHeal(pet.fightData.stats);
		
		if(this.action.hp)
			client.write(PetPackets.healHp(pet));
		if(this.action.mp)
			client.write(PetPackets.healMp(pet));
	}
	
	private doHeal(stats: Stats): void{
		if(this.action.isPerc){
			if(this.action.hp)
				stats.addHpPerc(this.action.hp);
			if(this.action.mp)
				stats.addMpPerc(this.action.mp);
		}
		else{
			if(this.action.hp)
				stats.addHp(this.action.hp);
			if(this.action.mp)
				stats.addMp(this.action.mp);
		}
	}
}
