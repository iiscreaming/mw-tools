import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { Random } from '../../Utils/Random';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionRemoveItem } from '../GameActionTypes';

/**
 * Remove an item from the player.
 */
export class RemoveItemExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionRemoveItem,
	){super(action);}
	
	public static parse(action: GameActionRemoveItem): GameActionExecutable{
		return new this(action);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		let baseItem = client.game.baseItems.get(this.action.baseItemId);
		
		if(!baseItem)
			throw Error('Unknown base item id ' + this.action.baseItemId);
		
		let amount = Random.fromJson(this.action.amount);
		client.player.items.removeItemAndSend(baseItem.id, amount);
	}
}
