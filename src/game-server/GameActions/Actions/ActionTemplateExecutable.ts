import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { Logger } from '../../Logger/Logger';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionTemplate } from '../GameActionTypes';
import { testFight } from './Templates/TestFight';
import { testShop } from './Templates/TestShop';

export type ActionTemplateCallback = (client: ClientConnectionWithPlayer, params?: Record<string, unknown>) => void;

/**
 * Executes custom functions.
 */
export class ActionTemplateExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionTemplate,
		protected callback: ActionTemplateCallback,
	){super(action);}
	
	public static parse(action: GameActionTemplate): GameActionExecutable{
		switch(action.template){
			case 'testFight': return new this(action, testFight);
			case 'testShop': return new this(action, testShop);
			default:
				Logger.error('Unknown action template', action);
				throw Error('Unknown action template ' + action.template);
		}
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		this.callback(client, this.action.params);
	}
}
