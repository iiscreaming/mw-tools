import { Fight } from '../../../GameState/Fight/Fight';
import { FightMember } from '../../../GameState/Fight/FightMember';
import { Monster } from '../../../GameState/Monster/Monster';
import type { ActionTemplateCallback } from '../ActionTemplateExecutable';

export const testFight: ActionTemplateCallback = client => {
	let fight = new Fight(client.game);
	let self = new FightMember(fight, client.player);
	fight.members.set(self.base.id, self);
	client.player.fightData.currentFight = fight;
	
	for(let i = 0; i < 3; ++i){
		let monster = new Monster();
		monster.id = 0x80000000 + i;
		monster.name = 'Angry Shroom';
		monster.file = 222;
		monster.fightData.stats.agi = 1;
		monster.fightData.stats.int = 1;
		monster.fightData.stats.sta = 1;
		monster.fightData.stats.str = 1;
		monster.fightData.stats.healHp();
		monster.fightData.stats.healMp();
		let enemy = new FightMember(fight, monster);
		enemy.location = 10 + i;
		fight.members.set(enemy.base.id, enemy);
	}
	
	if(client.player.activePet){
		let pet = new FightMember(fight, client.player.activePet);
		pet.location = 1;
		fight.members.set(pet.base.id, pet);
	}
	
	fight.start();
};
