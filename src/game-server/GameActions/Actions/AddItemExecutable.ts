import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { Random } from '../../Utils/Random';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionAddItem } from '../GameActionTypes';

/**
 * Give an item to the player.
 */
export class AddItemExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionAddItem,
	){super(action);}
	
	public static parse(action: GameActionAddItem): GameActionExecutable{
		return new this(action);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		let baseItem = client.game.baseItems.get(this.action.baseItemId);
		
		if(!baseItem)
			throw Error('Unknown base item id ' + this.action.baseItemId);
		
		let amount = Random.fromJson(this.action.amount);
		
		if(!client.player.items.inventory.hasSpaceFor(baseItem, amount))
			throw Error('Player does not have space, add condition check.');
		
		client.player.items.addItemAndSend(baseItem, amount);
	}
}
