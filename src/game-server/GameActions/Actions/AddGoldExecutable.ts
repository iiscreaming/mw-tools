import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { Random } from '../../Utils/Random';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionGold } from '../GameActionTypes';

/**
 * Give gold to the player.
 */
export class AddGoldExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionGold,
	){super(action);}
	
	public static parse(action: GameActionGold): GameActionExecutable{
		return new this(action);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		let amount = Random.fromJson(this.action.amount);
		client.player.items.addGoldAndSend(amount);
	}
}
