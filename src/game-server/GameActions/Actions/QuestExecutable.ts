import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { MessagePackets } from '../../Responses/MessagePackets';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionQuest } from '../GameActionTypes';

/**
 * Add, update or remove a quest.
 */
export class QuestExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionQuest,
	){super(action);}
	
	public static parse(action: GameActionQuest): GameActionExecutable{
		return new this(action);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		let {add, remove, set} = this.action;
		
		if(remove)
			client.player.quests.removeAndSend(remove);
		
		if(add){
			client.player.quests.addAndSend(add.quest, add.stage);
			client.write(MessagePackets.showMessage('A new quest was added!'));
		}
		
		if(set)
			client.player.quests.get(set.quest)?.setStageAndSend(set.stage);
	}
}
