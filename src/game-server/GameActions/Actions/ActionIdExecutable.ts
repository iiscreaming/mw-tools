import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameActionCache } from '../GameActionCache';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionId } from '../GameActionTypes';

/**
 * Executes actions by id.
 */
export class ActionIdExecutable extends GameActionExecutable{
	protected constructor(
		public readonly action: GameActionId,
	){super(action);}
	
	public static parse(action: GameActionId): GameActionExecutable{
		return new this(action);
	}
	
	protected run(client: ClientConnectionWithPlayer): void{
		GameActionCache.getInstance().get(this.action.id)?.execute(client);
	}
}
