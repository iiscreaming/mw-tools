import type { ClientConnectionWithPlayer } from '../ClientConnection';
import { Logger } from '../Logger/Logger';
import { GameActionParser } from './GameActionParser';
import type { GameActionSingle } from './GameActionTypes';
import type { GameConditionExecutable } from './GameConditionExecutable';
import { GameConditionParser } from './GameConditionParser';

/**
 * An action that can be executed.
 */
export abstract class GameActionExecutable{
	/**
	 * Executable action that does nothing.
	 */
	public static readonly noop: GameActionExecutable = new class extends GameActionExecutable{
		public constructor(){super({type: 'noop'});}
		public execute(): boolean{return true;}
		protected run(): void{return;}
	};
	
	/**
	 * Will be checked before executing the action.
	 */
	protected condition: GameConditionExecutable | null;
	
	/**
	 * Called when the condition fails.
	 */
	protected else: GameActionExecutable | null;
	
	protected constructor(
		protected action: GameActionSingle,
	){
		this.condition = action.condition ? GameConditionParser.parse(action.condition) : null;
		this.else = action.else ? GameActionParser.parse(action.else) : null;
	}
	
	/**
	 * Execute this action. Returns the result of the condition.
	 * @param client
	 */
	public execute(client: ClientConnectionWithPlayer): boolean{
		try{
			let allowed = this.condition ? this.condition.execute(client) : true;
			
			if(allowed)
				this.run(client);
			else
				this.else?.execute(client);
			
			return allowed;
		}
		catch(up: unknown){
			Logger.error('Error during execution of game action', this.action, up);
			throw up;
		}
	}
	
	/**
	 * The logic for this action.
	 * @param client
	 */
	protected abstract run(client: ClientConnectionWithPlayer): void;
}
