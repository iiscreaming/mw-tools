import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { Logger } from '../../Logger/Logger';
import { GameConditionExecutable } from '../GameConditionExecutable';
import type { GameConditionTemplate } from '../GameConditionTypes';

export type ConditionTemplateCallback = (client: ClientConnectionWithPlayer, params?: Record<string, unknown>) => boolean;

/**
 * Executes custom conditions.
 */
export class ConditionTemplateExecutable extends GameConditionExecutable{
	protected constructor(
		public readonly condition: GameConditionTemplate,
		protected callback: ConditionTemplateCallback,
	){super(condition);}
	
	public static parse(condition: GameConditionTemplate): GameConditionExecutable{
		switch(condition.template){
			default:
				Logger.error('Unknown condition template', condition);
				throw Error('Unknown condition template ' + condition.template);
		}
	}
	
	protected run(client: ClientConnectionWithPlayer): boolean{
		return this.callback(client, this.condition.params);
	}
}
