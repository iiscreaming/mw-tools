import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameConditionExecutable } from '../GameConditionExecutable';
import type { GameConditionGold } from '../GameConditionTypes';

/**
 * Checks if the player has the given amount of gold (or more).
 */
export class ConditionGoldExecutable extends GameConditionExecutable{
	protected constructor(
		protected condition: GameConditionGold,
	){super(condition);}
	
	public static parse(condition: GameConditionGold): GameConditionExecutable{
		return new this(condition);
	}
	
	protected run(client: ClientConnectionWithPlayer): boolean{
		return client.player.items.gold >= this.condition.amount;
	}
}
