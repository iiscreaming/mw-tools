import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameConditionExecutable } from '../GameConditionExecutable';
import type { GameConditionItemSpace } from '../GameConditionTypes';

/**
 * Checks if the player has space for an item.
 * Count defaults to 1. With no baseItemId it will check the amount of slots.
 */
export class ConditionItemSpaceExecutable extends GameConditionExecutable{
	private baseItemId: number | null;
	private count: number;
	
	protected constructor(
		protected condition: GameConditionItemSpace,
	){
		super(condition);
		this.baseItemId = condition.baseItemId ?? null;
		this.count = condition.count ?? 1;
	}
	
	public static parse(condition: GameConditionItemSpace): GameConditionExecutable{
		return new this(condition);
	}
	
	protected run(client: ClientConnectionWithPlayer): boolean{
		if(this.baseItemId === null)
			return client.player.items.inventory.freeSize >= this.count;
		
		let baseItem = client.game.baseItems.get(this.baseItemId);
		
		if(!baseItem)
			throw Error('Unknown base item id ' + this.baseItemId);
		
		return client.player.items.inventory.hasSpaceFor(baseItem, this.count);
	}
}
