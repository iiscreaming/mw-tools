import { Random } from '../../Utils/Random';
import { GameConditionExecutable } from '../GameConditionExecutable';
import type { GameConditionRandom } from '../GameConditionTypes';

/**
 * Has a percentage chance of returning true.
 */
export class ConditionRandomExecutable extends GameConditionExecutable{
	private chance: number;
	
	protected constructor(
		protected condition: GameConditionRandom,
	){
		super(condition);
		this.chance = condition.chance / 100;
	}
	
	public static parse(condition: GameConditionRandom): GameConditionExecutable{
		return new this(condition);
	}
	
	protected run(): boolean{
		return Random.chance(this.chance);
	}
}
