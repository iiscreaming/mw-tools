import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameConditionExecutable } from '../GameConditionExecutable';
import type { GameConditionQuest } from '../GameConditionTypes';

/**
 * Checks if the player has a quest.
 */
export class ConditionQuestExecutable extends GameConditionExecutable{
	protected constructor(
		protected condition: GameConditionQuest,
	){super(condition);}
	
	public static parse(condition: GameConditionQuest): GameConditionExecutable{
		return new this(condition);
	}
	
	protected run(client: ClientConnectionWithPlayer): boolean{
		if('stage' in this.condition){
			let quest = client.player.quests.get(this.condition.quest);
			return quest?.stageIndex === this.condition.stage;
		}
		else{
			return client.player.quests.has(this.condition.quest);
		}
	}
}
