import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameConditionExecutable } from '../GameConditionExecutable';
import { GameConditionParser } from '../GameConditionParser';
import type { GameConditionAnd, GameConditionSingle } from '../GameConditionTypes';

/**
 * Checks if all conditions are true.
 */
export class AndExecutable extends GameConditionExecutable{
	protected constructor(
		protected condition: GameConditionSingle,
		protected conditions: GameConditionExecutable[],
	){super(condition);}
	
	public static parse(condition: GameConditionAnd): GameConditionExecutable{
		let conditions = condition.conditions.map(con => GameConditionParser.parse(con));
		return new this(condition, conditions);
	}
	
	protected run(client: ClientConnectionWithPlayer): boolean{
		return this.conditions.every(con => con.execute(client));
	}
}
