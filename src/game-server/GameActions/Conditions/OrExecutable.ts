import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameConditionExecutable } from '../GameConditionExecutable';
import { GameConditionParser } from '../GameConditionParser';
import type { GameConditionOr, GameConditionSingle } from '../GameConditionTypes';

/**
 * Checks if at least one condition is true.
 */
export class OrExecutable extends GameConditionExecutable{
	protected constructor(
		protected condition: GameConditionSingle,
		protected conditions: GameConditionExecutable[],
	){super(condition);}
	
	public static parse(condition: GameConditionOr): GameConditionExecutable{
		let conditions = condition.conditions.map(con => GameConditionParser.parse(con));
		return new this(condition, conditions);
	}
	
	protected run(client: ClientConnectionWithPlayer): boolean{
		return this.conditions.some(con => con.execute(client));
	}
}
