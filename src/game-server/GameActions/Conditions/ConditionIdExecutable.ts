import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import { GameConditionCache } from '../GameConditionCache';
import { GameConditionExecutable } from '../GameConditionExecutable';
import type { GameConditionId } from '../GameConditionTypes';

/**
 * Checks a condition by id.
 */
export class ConditionIdExecutable extends GameConditionExecutable{
	protected constructor(
		public readonly condition: GameConditionId,
	){super(condition);}
	
	public static parse(condition: GameConditionId): GameConditionExecutable{
		return new this(condition);
	}
	
	protected run(client: ClientConnectionWithPlayer): boolean{
		return GameConditionCache.getInstance().get(this.condition.id)?.execute(client) ?? false;
	}
}
