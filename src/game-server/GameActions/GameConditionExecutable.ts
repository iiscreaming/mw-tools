import type { ClientConnectionWithPlayer } from '../ClientConnection';
import { Logger } from '../Logger/Logger';
import type { GameConditionSingle } from './GameConditionTypes';

/**
 * A condition that can be executed.
 */
export abstract class GameConditionExecutable{
	/**
	 * Condition that always returns true.
	 */
	public static readonly true: GameConditionExecutable = new class extends GameConditionExecutable{
		public constructor(){super(null);}
		public execute(): boolean{return true;}
		protected run(): boolean{return true;}
	};
	
	/**
	 * Condition that always returns false.
	 */
	public static readonly false: GameConditionExecutable = new class extends GameConditionExecutable{
		public constructor(){super(null);}
		public execute(): boolean{return false;}
		protected run(): boolean{return false;}
	};
	
	protected constructor(
		protected condition: GameConditionSingle,
	){}
	
	/**
	 * Call the condition.
	 * @param client
	 */
	public execute(client: ClientConnectionWithPlayer): boolean{
		try{
			let result = this.run(client);
			return this.condition?.not ? !result : result;
		}
		catch(up: unknown){
			Logger.error('Error during execution of game condition', this.condition, up);
			throw up;
		}
	}
	
	/**
	 * The logic for this condition.
	 * @param client
	 */
	protected abstract run(client: ClientConnectionWithPlayer): boolean;
}
