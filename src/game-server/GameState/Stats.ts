export class Stats{
	public sta: number = 0;
	
	public int: number = 0;
	
	public str: number = 0;
	
	public agi: number = 0;
	
	public hp: number = 0;
	
	public mp: number = 0;
	
	//TODO figure out calcs
	public get totalHp(): number{
		return 100 + this.sta * 10;
	}
	
	public get totalMp(): number{
		return 100 + this.int * 10;
	}
	
	public get attack(): number{
		return 10 + this.str;
	}
	
	public get speed(): number{
		return 10 + this.agi;
	}
	
	/**
	 * Set hp to max.
	 */
	public healHp(): void{
		this.hp = this.totalHp;
	}
	
	/**
	 * Set mp to max.
	 */
	public healMp(): void{
		this.mp = this.totalMp;
	}
	
	/**
	 * Add/subtract hp. Stays within 0 to total hp range.
	 * @param hp
	 */
	public addHp(hp: number): void{
		this.hp = Math.max(0, Math.min(this.hp + hp, this.totalHp));
	}
	
	/**
	 * Add/subtract mp. Stays within 0 to total mp range.
	 * @param hp
	 */
	public addMp(mp: number): void{
		this.mp = Math.max(0, Math.min(this.mp + mp, this.totalMp));
	}
	
	/**
	 * Add/subtract hp by percentage of the players total health.
	 * @param value Value from 0 and 100
	 */
	public addHpPerc(value: number): void{
		this.addHp(this.totalHp * value / 100);
	}
	
	/**
	 * Add/subtract mp by percentage of the players total mana.
	 * @param value Value from 0 and 100
	 */
	public addMpPerc(value: number): void{
		this.addMp(this.totalMp * value / 100);
	}
}
