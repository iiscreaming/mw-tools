import type { Direction } from '../../Enums/Direction';
import type { GameMap } from '../Map/GameMap';

export abstract class IndividualMapData{
	public x: number = 0;
	
	public y: number = 0;
	
	public destX: number = 0;
	
	public destY: number = 0;
	
	public direction: Direction = 0;
	
	public canWalk: boolean = true;
	
	public map: GameMap | null = null;
}
