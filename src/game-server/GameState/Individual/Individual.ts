import type { IndividualFightData } from './IndividualFightData';
import type { IndividualMapData } from './IndividualMapData';

export abstract class Individual{
	public id: number = 0;
	
	public name: string = '';
	
	public file: number = 0;
	
	public abstract mapData: IndividualMapData | null;
	
	public abstract fightData: IndividualFightData | null;
}
