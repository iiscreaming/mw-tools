import { Resist } from '../Resist';
import type { SkillData } from '../SkillData';
import { Stats } from '../Stats';

export abstract class IndividualFightData{
	public stats: Stats = new Stats();
	
	public resist: Resist = new Resist();
	
	public skills: SkillData[] = [];
}
