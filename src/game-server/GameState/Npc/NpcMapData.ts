import { IndividualMapData } from '../Individual/IndividualMapData';

export class NpcMapData extends IndividualMapData{
	public canWalk: boolean = false;
}
