import type { ClientConnectionWithPlayer } from '../../ClientConnection';
import type { GameActionExecutable } from '../../GameActions/GameActionExecutable';
import { Individual } from '../Individual/Individual';
import { NpcMapData } from './NpcMapData';

// NPC IDs must be in the 0x80000000 range.
export class Npc extends Individual{
	public defaultMap: number | null = null;
	
	public fightData: null = null;
	
	public mapData: NpcMapData = new NpcMapData();
	
	public action: GameActionExecutable | null = null;
	
	/**
	 * Called when the client talks to this npc.
	 * @param client
	 */
	public onTalk(client: ClientConnectionWithPlayer): void{
		if(!client.player)
			return;
		
		client.player.memory.activeNpc = this;
		this.action?.execute(client);
	}
	
	/**
	 * Called when the client closes a dialog window.
	 * @param client
	 * @param option
	 */
	public onCloseDialog(client: ClientConnectionWithPlayer, option: number): void{
		if(client.player.memory.activeNpc !== this)
			return;
		
		let npcOption = client.player.memory.npcOptions?.[option];
		client.player.memory.npcOptions = null;
		npcOption?.execute(client);
	}
}
