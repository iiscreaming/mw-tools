import { IndividualMapData } from '../Individual/IndividualMapData';

export class PlayerMapData extends IndividualMapData{
	/**
	 * Sets the player coordinates randomly in the area around the given coordinates.
	 * @param x
	 * @param y
	 * @param minDistance
	 * @param maxDistance
	 */
	public setNearPoint(x: number, y: number, minDistance: number = 3, maxDistance: number = 6): void{
		let direction = Math.random() * 2 * Math.PI;
		let distance = minDistance + Math.random() * (maxDistance - minDistance);
		
		x += distance * Math.cos(direction) * 16;
		y += distance * Math.sin(direction) * 8;
		
		this.x = this.destX = Math.round(x);
		this.y = this.destY = Math.round(y);
	}
}
