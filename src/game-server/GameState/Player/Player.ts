import type { ClientConnection, ClientConnectionWithPlayer } from '../../ClientConnection';
import type { CharacterClass, CharacterGender, CharacterRace } from '../../Enums/CharacterClass';
import { PlayerEffect } from '../../Enums/PlayerEffect';
import { MapPackets } from '../../Responses/MapPackets';
import { Bitfield } from '../../Utils/Bitfield';
import type { Game } from '../Game';
import type { Guild } from '../Guild';
import { Individual } from '../Individual/Individual';
import { Level } from '../Level';
import type { Pet } from '../Pet/Pet';
import { PlayerQuests } from '../Quest/PlayerQuests';
import { PlayerFightData } from './PlayerFightData';
import { PlayerItems } from './PlayerItems';
import { PlayerMapData } from './PlayerMapData';
import { PlayerMemory } from './PlayerMemory';
import { PlayerMisc } from './PlayerMisc';
import { PlayerTitles } from './PlayerTitles';

export class Player extends Individual{
	public client: ClientConnectionWithPlayer | null = null;
	
	public fightData: PlayerFightData = new PlayerFightData();
	
	public mapData: PlayerMapData = new PlayerMapData();
	
	public titles: PlayerTitles = new PlayerTitles();
	
	public quests: PlayerQuests;
	
	public gender: CharacterGender = 0;
	
	public race: CharacterRace = 0;
	
	public effect: Bitfield = new Bitfield(PlayerEffect.None);
	
	public level: Level = Level.fromLevel(1);
	
	public unusedStats: number = 0;
	
	public guild: Guild | null = null;
	
	public misc: PlayerMisc = new PlayerMisc();
	
	public get class(): CharacterClass{
		return this.race * 2 + this.gender;
	}
	
	public items: PlayerItems = new PlayerItems(this);
	
	public pets: Pet[] = [];
	
	public activePet: Pet | null = null;
	
	public memory: PlayerMemory = new PlayerMemory();
	
	public constructor(
		public game: Game,
		client: ClientConnection | null,
	){
		super();
		
		if(client){
			if(client.player)
				throw Error('Client is already assigned a player.');
			
			client.player = this;
			this.client = client as ClientConnectionWithPlayer;
		}
		
		this.quests = new PlayerQuests(this);
	}
	
	/**
	 * Sends the map and population data to the client.
	 */
	public enterMap(first: boolean = false): void{
		if(!this.client)
			return;
		
		this.client.write(
			MapPackets.locationData(this),
			MapPackets.mapPopulation(this),
			first ? MapPackets.sceneEnter : MapPackets.sceneChange,
		);
	}
	
	/**
	 * Sets the file based on race, gender and reborn.
	 */
	public useDefaultFile(): void{
		this.file = this.class + 1;
		
		if(this.level.reborn > 0)
			this.file += 10;
	}
}
