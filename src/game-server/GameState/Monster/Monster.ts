import { Individual } from '../Individual/Individual';
import { MonsterFightData } from './MonsterFightData';

export class Monster extends Individual{
	public fightData: MonsterFightData = new MonsterFightData();
	
	public mapData: null = null;
}
