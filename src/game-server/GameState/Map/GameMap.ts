import { mapCellData, mapLinkData } from '../../Data/MapLinks';
import type { Weather } from '../../Enums/Weather';
import type { Npc } from './../Npc/Npc';
import type { Player } from './../Player/Player';
import { MapLink } from './MapLink';

export class GameMap{
	public id: number = 0;
	
	public name: string = '';
	
	public file: number = 0;
	
	public musicFile: number = 0;
	
	public minimapFile: number = 0;
	
	public minimapEnabled: boolean = false;
	
	public fightMusicFile: number = 0;
	
	public fightBackgroundFile: number = 0;
	
	public weather: Weather = 0;
	
	public npcs: Npc[] = [];
	
	public players: Player[] = [];
	
	// TODO tidy up
	private width: number = 0;
	private height: number = 0;
	private links: Map<number, MapLink> | null = null;
	private cellData: Buffer | null = null;
	
	/**
	 * Set up the map links. Called once after all maps are loaded.
	 * @param maps
	 */
	public initLinks(maps: Map<number, GameMap>): void{
		this.cellData = mapCellData.get(this.file) ?? null;
		let links = mapLinkData.get(this.file);
		
		if(!links)
			return;
		
		this.width = links.width;
		this.height = links.height;
		this.links = new Map(links.links.map(l => [l.id, new MapLink(l, maps)]));
	}
	
	/**
	 * Adds player to this map.
	 * Removes player from previous map.
	 * @param player
	 */
	public addPlayer(player: Player): void{
		if(player.mapData.map !== this){
			if(player.mapData.map)
				player.mapData.map.removePlayer(player);
			
			player.mapData.map = this;
		}
		
		if(this.players.includes(player))
			return;
		
		this.players.push(player);
	}
	
	/**
	 * Remove player from this map.
	 * Does not unset player.mapData.map.
	 * @param player
	 */
	public removePlayer(player: Player): void{
		let index = this.players.indexOf(player);
		
		if(index !== -1)
			this.players.splice(index, 1);
	}
	
	/**
	 * Get the map link for the given coordinates.
	 * @param x
	 * @param y
	 */
	public getMapLink(x: number, y: number): MapLink | null{
		if(this.cellData === null || this.links === null)
			return null;
		
		let row = Math.floor(y / 8);
		let col = Math.floor(x / 16);
		let pos = row * (this.width / 16) + col;
		let value = this.cellData.readUInt8(pos);
		
		if((value & 0x40) === 0)
			return null;
		
		let id = value & 0x3F;
		return this.links.get(id) ?? null;
	}
}
