import type { MapLinkJson } from '../../Data/MapLinks';
import { Random } from '../../Utils/Random';
import type { GameMap } from './GameMap';

export class MapLink{
	public readonly id: number;
	public readonly dest: GameMap | null;
	private readonly startX: number;
	private readonly startY: number;
	private readonly endX: number;
	private readonly endY: number;
	
	public constructor(json: MapLinkJson, maps: Map<number, GameMap>){
		this.id = json.id;
		this.startX = json.x1 * 16;
		this.startY = json.y1 * 8;
		this.endX = json.x2 * 16;
		this.endY = json.y2 * 8;
		
		// For now just set the destination to the first that matches the file
		// Later add a way to override this for custom maps
		this.dest = [...maps.values()].find(m => m.file === json.map) ?? null;
	}
	
	public getDestinationCoordinates(): [x: number, y: number]{
		return [Random.int(this.startX, this.endX), Random.int(this.startY, this.endY)];
	}
}
