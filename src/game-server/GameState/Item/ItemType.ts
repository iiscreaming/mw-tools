export enum ItemType{
	None		= 0,
	Equipment	= 1,
	Consumable	= 2,
	Usable		= 3,
}
