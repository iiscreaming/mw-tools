import { BaseItemCollection } from '../Database/Collections/BaseItem/BaseItemCollection';
import type { BaseItem } from '../Database/Collections/BaseItem/BaseItemTypes';
import { GameDataCollection } from '../Database/Collections/GameData/GameDataCollection';
import { NpcCollection } from '../Database/Collections/Npc/NpcCollection';
import { GameActionCache } from '../GameActions/GameActionCache';
import { GameConditionCache } from '../GameActions/GameConditionCache';
import type { Fight } from './Fight/Fight';
import { GameMap } from './Map/GameMap';
import type { Npc } from './Npc/Npc';
import type { Player } from './Player/Player';
import type { BaseQuest } from './Quest/BaseQuest';

export class Game{
	public maps: Map<number, GameMap> = new Map();
	
	public baseItems: Map<number, BaseItem> = new Map();
	
	public baseQuests: Map<number, BaseQuest> = new Map();
	
	public npcs: Map<number, Npc> = new Map();
	
	public players: Map<number, Player> = new Map();
	
	public fights: Set<Fight> = new Set();
	
	public async init(): Promise<void>{
		await GameActionCache.getInstance().ready;
		await GameConditionCache.getInstance().ready;
		await this.loadMaps();
		await this.loadNpcs();
		await this.loadBaseItems();
		await this.loadBaseQuests();
	}
	
	/**
	 * Add player to the game.
	 * @param player
	 */
	public addPlayer(player: Player): void{
		if(!player.mapData.map){
			console.error('Could not add player, unknown map.');
			return;
		}
		
		player.mapData.map.addPlayer(player);
		this.players.set(player.id, player);
	}
	
	/**
	 * Remove player from the game.
	 * @param player
	 */
	public removePlayer(player: Player): void{
		if(player.mapData.map)
			player.mapData.map.removePlayer(player);
		
		this.players.delete(player.id);
	}
	
	/**
	 * Add an npc to the game.
	 * @param npc
	 * @param mapId
	 */
	public addNpc(npc: Npc, mapId: number | null): void{
		if(npc.mapData.map)
			this.removeNpc(npc);
		
		let map = mapId ? this.maps.get(mapId) : null;
		
		if(map){
			npc.mapData.map = map;
			map.npcs.push(npc);
		}
		
		this.npcs.set(npc.id, npc);
	}
	
	/**
	 * Remove an npc from the game.
	 * @param npc
	 */
	public removeNpc(npc: Npc): void{
		if(npc.mapData.map){
			let index = npc.mapData.map.npcs.indexOf(npc);
			
			if(index !== -1)
				npc.mapData.map.npcs.splice(index, 1);
			
			npc.mapData.map = null;
		}
		
		this.npcs.delete(npc.id);
	}
	
	/**
	 * Load all static maps.
	 */
	private async loadMaps(): Promise<void>{
		let mapDataList = await (await (GameDataCollection.getInstance()).getMaps()).get();
		
		for(let mapData of mapDataList){
			let map = new GameMap();
			map.id = mapData.id;
			map.file = mapData.file;
			map.name = mapData.name;
			map.minimapEnabled = mapData.minimapFile !== -1;
			map.minimapFile = mapData.minimapFile;
			map.fightBackgroundFile = mapData.fightBackgroundFile;
			this.maps.set(map.id, map);
		}
		
		for(let map of this.maps.values()){
			map.initLinks(this.maps);
		}
	}
	
	/**
	 * Load all static npcs.
	 */
	private async loadNpcs(): Promise<void>{
		let npcs = await NpcCollection.getInstance().getAll();
		npcs.forEach(npc => this.addNpc(npc, npc.defaultMap));
	}
	
	/**
	 * Load all base items.
	 */
	private async loadBaseItems(): Promise<void>{
		let items = await BaseItemCollection.getInstance().getAll();
		items.forEach(item => this.baseItems.set(item.id, item));
	}
	
	/**
	 * Load all base quests.
	 */
	private async loadBaseQuests(): Promise<void>{
		this.baseQuests.set(1, {
			id: 1,
			clientId: 10150,
			stages: new Map([[0, {
				requirements: '',
				reward: 'Nothing',
				situation: 'Actually, go talk to the teleporter in Blython',
			}]]),
		});
	}
}
