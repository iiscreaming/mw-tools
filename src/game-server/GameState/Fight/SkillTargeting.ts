import { skillProperties } from '../../Data/SkillProperties';
import { FightActionCommand } from '../../Enums/FightActionCommand';
import { FightEffect } from '../../Enums/FightEffect';
import { SkillGroup } from '../../Enums/SkillGroup';
import type { SkillData } from '../SkillData';
import type { FightAction } from './FightAction';
import type { FightMember } from './FightMember';

export class SkillTargeting{
	/**
	 * Get a list of targets for the given action.
	 * Targets faster characters first.
	 * @param action
	 */
	public static getBySpeed(action: FightAction): FightMember[]{
		if(action.type !== FightActionCommand.Skill)
			return [];
		
		let skillData = action.member.base.fightData.skills.find(s => s.id === action.detail);
		
		if(!skillData)
			return [];
		
		let targetEnemey = skillProperties[skillData.id].enemy;
		let count = this.getCount(skillData) - 1; // Remove 1 for main target
		let skipDead = this.shouldSkipDead(skillData);
		
		let possibleTargets = action.fight.getSide(action.member, targetEnemey);
		let targets: FightMember[] = [];
		
		if(action.target && !action.target.effect.has(FightEffect.Dead))
			targets.push(action.target);
		
		for(let pTarget of possibleTargets){
			if(count <= 0)
				break;
			
			if(pTarget === action.target)
				continue;
			
			if(skipDead && pTarget.effect.has(FightEffect.Dead))
				continue;
			
			targets.push(pTarget);
			--count;
		}
		
		return targets;
	}
	
	/**
	 * Get the number of people targetted by this skill.
	 * @todo calculate based on level.
	 * @param skillData
	 */
	private static getCount(skillData: SkillData): number{
		let skill = skillProperties[skillData.id];
		
		if(skill.single)
			return 1;
		
		return skill.targets;
	}
	
	/**
	 * Whether or not dead characters are skipped over.
	 * @todo do skills actually skip over dead characters or just not affect them?
	 * @param skillData
	 */
	private static shouldSkipDead(skillData: SkillData): boolean{
		let skill = skillProperties[skillData.id];
		return skill.group !== SkillGroup.HealOther;
	}
}
