import type { FightMember } from './FightMember';

/**
 * Execute a melee FightAction.
 * @param source
 * @param target
 */
export function executeMeleeAction(source: FightMember, target: FightMember): Buffer{
	let counter = 0;
	
	if(calculateMiss(source, target))
		return getDataMelee(false, 0, 0, counter);
	
	let damage = calculateMeleeDamage(source, target);
	let reflect = 0;
	
	target.base.fightData.stats.addHp(-damage);
	
	return getDataMelee(true, damage, reflect, counter);
}

/**
 * Execute a melee combo FightAction.
 * @param combos
 * @param source
 * @param target
 */
export function executeMeleeComboAction(combos: number, source: FightMember, target: FightMember): Buffer{
	let totalDamage = 0;
	let comboData: [number, number, number][] = [];
	let damage = calculateMeleeDamage(source, target);
	
	while(combos--){
		let miss = calculateMiss(source, target);
		let reflect = 0;
		let counter = 0;
		
		comboData.push([miss ? 0 : damage, reflect, counter]);
		
		if(!miss)
			totalDamage += damage;
		
		damage = Math.max(Math.round(damage / 2), 1);
	}
	
	target.base.fightData.stats.addHp(-totalDamage);
	
	return getDataMeleeCombo(comboData);
}

/**
 * Calculates the melee damage the source would do against the target.
 * Does not include: critical, combo, missing.
 * @param source
 * @param target
 */
function calculateMeleeDamage(source: FightMember, target: FightMember): number{
	let atk = source.base.fightData.stats.attack;
	
	/* TODO:
	Source:
		- Resist.Pierce
		- Resist.Berserk
		- Status: Enhance
	Enemy:
		- Resist.Defense
		- Resist.MeleeResist
		- Status: Protect
	Both:
		- Equipment
		- Shapeshift
	*/
	
	return atk;
}

/**
 * Returns true (miss) or false (hit) based on the stats.
 * @param source
 * @param target
 */
function calculateMiss(source: FightMember, target: FightMember): boolean{
	let hitRate = source.base.fightData.resist.hitRate / 100;
	let dodgeRate = target.base.fightData.resist.dodgeRate / 100;
	let hitChance = .9 * (hitRate - dodgeRate);
	
	return Math.random() > hitChance;
}

/**
 * Returns the combo count based on the stats, 1 means no combo.
 * @param source
 */
export function calculateComboCount(source: FightMember): number{
	let rate = source.base.fightData.resist.comboRate / 100;
	let hit = source.base.fightData.resist.comboHit + 1;
	
	if(hit > 7)
		hit = 7;
	
	return Math.random() < rate ? hit : 1;
}

/**
 * Melee specific data.
 */
function getDataMelee(hit: boolean, damage: number, reflect: number, counter: number): Buffer{
	let data = Buffer.alloc(13);
	data.writeUInt8(hit ? 1 : 0, 0);
	data.writeUInt32LE(damage, 1);
	data.writeUInt32LE(reflect, 5);
	data.writeUInt32LE(counter, 9);
	
	return data;
}

/**
 * Melee combo data.
 * @param combo [damage, reflect, counter][<=7]
 */
function getDataMeleeCombo(combo: [number, number, number][]): Buffer{
	let data = Buffer.alloc(1 + 12 * combo.length);
	data.writeUInt8(combo.length, 0);
	
	for(let i = 0; i < combo.length; i++){
		let offset = 1 + 12 * i;
		data.writeUInt32LE(combo[i][0], offset);
		data.writeUInt32LE(combo[i][1], offset + 4);
		data.writeUInt32LE(combo[i][2], offset + 8);
	}
	
	return data;
}
