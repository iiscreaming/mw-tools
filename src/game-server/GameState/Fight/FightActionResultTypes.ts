import type { FightActionType } from '../../Enums/FightActionType';

export type FightActionResult = {
	type: FightActionType,
	source: number,
	target: number,
	detail?: number,
	status?: FightActionResultStatus[],
	magic?: FightActionResultMagic[] | null,
	data?: Buffer | null,
};

export type FightActionResultStatus = {
	id: number,
	totalHp: number,
	hp: number,
	totalMp: number,
	mp: number,
	speed: number,
	effect: number,
};

export type FightActionResultMagic = {
	id: number,
	damage: number,
	repel: number,
};
