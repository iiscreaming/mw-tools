import { FightEffect } from '../../Enums/FightEffect';
import { FightMemberType } from '../../Enums/FightMemberType';
import { Bitfield } from '../../Utils/Bitfield';
import type { Monster } from '../Monster/Monster';
import { Pet } from '../Pet/Pet';
import { Player } from '../Player/Player';
import { EffectApply } from './EffectApply';
import type { Fight } from './Fight';
import { FightAction } from './FightAction';

export class FightMember{
	/**
	 * Location in the fight view.
	 * 16 17	9 8
	 * 12 13	5 4
	 * 10 11	1 0
	 * 14 15	3 2
	 * 18 19	7 6
	 */
	public location: number = 0;
	
	/**
	 * Not sure yet what it is used for.
	 */
	public active: boolean = true;
	
	/**
	 * Not sure yet what it is used for.
	 */
	public readonly type: FightMemberType;
	
	/**
	 * FightEffect, multiple can be combined.
	 */
	public readonly effect: Bitfield = new Bitfield(FightEffect.None);
	
	/**
	 * How many more turns the effects should last.
	 */
	public readonly effectCounters: Map<FightEffect, number> = new Map();
	
	/**
	 * The turn goes to the fight member with the lowest nextTurn.
	 * After every turn, increases with 1 / speed.
	 */
	public nextTurn: number;
	
	/**
	 * The most recent specified action.
	 */
	public readonly action: FightAction;
	
	public constructor(
		public readonly fight: Fight,
		public readonly base: Player | Pet | Monster,
	){
		this.action = new FightAction(fight, this);
		this.nextTurn = 1 / base.fightData.stats.speed;
		
		if(base instanceof Player)
			this.type = FightMemberType.Player;
		else if(base instanceof Pet)
			this.type = FightMemberType.Pet;
		else
			this.type = FightMemberType.Monster;
	}
	
	/**
	 * Lower the counter of each effect, and remove it if it reaches 0.
	 */
	public doEffectTurn(): void{
		for(let [effect, count] of this.effectCounters){
			if(count === 0)
				continue;
			
			if(count === 1)
				this.effect.remove(effect);
			
			this.effectCounters.set(effect, count - 1);
		}
	}
	
	/**
	 * Get rid of counters of unused effects.
	 */
	public removeUnusedCounters(): void{
		for(let effect of EffectApply.all){
			if(!this.effect.has(effect))
				this.effect.remove(effect);
		}
	}
}
