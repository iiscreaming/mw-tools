import { skillProperties } from '../../Data/SkillProperties';
import { FightActionCommand } from '../../Enums/FightActionCommand';
import { FightActionType } from '../../Enums/FightActionType';
import { FightEffect } from '../../Enums/FightEffect';
import type { Fight } from './Fight';
import { calculateComboCount, executeMeleeAction, executeMeleeComboAction } from './FightActionMelee';
import type { FightActionResult, FightActionResultStatus } from './FightActionResultTypes';
import { FightActionSkill } from './FightActionSkill';
import type { FightMember } from './FightMember';

/**
 * FightAction has 1:1 relation to FightMember.
 * Holds what the player wants to do on their next turn.
 */
export class FightAction{
	/**
	 * The main fight command.
	 */
	public type: FightActionCommand = FightActionCommand.Melee;
	
	/**
	 * The character being targeted.
	 */
	public target: FightMember | null = null;
	
	/**
	 * Action detail. Can be skill id, item slot, or defended id.
	 */
	public detail: number = 0;
	
	public constructor(
		public readonly fight: Fight,
		public readonly member: FightMember,
	){}
	
	/**
	 * Executes this action.
	 * @param fight
	 */
	public execute(): FightActionResult | null{
		if(this.member.effect.any(FightEffect.Dead | FightEffect.Stun | FightEffect.Hypnotize))
			return null;
		
		if(this.member.effect.has(FightEffect.Chaos))
			return this.executeChaossedAttack();
		
		if(!this.target)
			return null;
		
		switch(this.type){
			case FightActionCommand.Melee:
				return this.executeMelee();
			case FightActionCommand.Skill:
				return this.executeSkill();
			case FightActionCommand.Item:
				return this.executeItem();
			case FightActionCommand.Defend:
				return this.executeDefend();
			case FightActionCommand.Dodge:
				return this.executeDodge();
			case FightActionCommand.Escape:
				return this.executeEscape();
			case FightActionCommand.ChangePet:
				return this.executeChangePet();
			case FightActionCommand.HP:
				return this.executeHP();
			case FightActionCommand.MP:
				return this.executeMP();
			case FightActionCommand.Auto:
				// Probably needs to be handled before we get to this class.
				return null;
			default:
				return null;
		}
	}
	
	/**
	 * Set the target of this action by id.
	 * @param id
	 */
	public setTarget(id: number): void{
		this.target = this.fight.members.get(id) ?? null;
	}
	
	/**
	 * Execute a melee action.
	 */
	private executeMelee(): FightActionResult | null{
		if(!this.target || this.target.effect.any(FightEffect.Dead | FightEffect.Stun))
			return null;
		
		let combos = calculateComboCount(this.member);
		
		let data = combos === 1
			? executeMeleeAction(this.member, this.target)
			: executeMeleeComboAction(combos, this.member, this.target);
		
		// TODO use copy of hp in FightMember, update dead-status in setter
		if(this.target.base.fightData.stats.hp === 0)
			this.target.effect.value = FightEffect.Dead;
		
		let result: FightActionResult = {
			type: combos === 1 ? FightActionType.Melee : FightActionType.MeleeCombo,
			source: this.member.base.id,
			target: this.target.base.id,
			status: [
				FightAction.getResultStatus(this.member),
				FightAction.getResultStatus(this.target),
			],
			data,
		};
		
		return result;
	}
	
	/**
	 * Execute a skill action.
	 */
	private executeSkill(): FightActionResult | null{
		if(!this.target)
			return null;
		
		let skillData = this.member.base.fightData.skills.find(s => s.id === this.detail);
		
		if(!skillData)
			return null;
		
		let skill = skillProperties[skillData.id];
		let cost = Math.floor(skill.mp + skill.mpAdd * skillData.exp);
		
		if(cost > this.member.base.fightData.stats.mp)
			return null;
		
		this.member.base.fightData.stats.addMp(-cost);
		
		return FightActionSkill.execute(this, skillData);
	}
	
	/**
	 * Execute a use item action.
	 */
	private executeItem(): FightActionResult | null{
		return null;
	}
	
	/**
	 * Execute a defend action.
	 */
	private executeDefend(): FightActionResult | null{
		return null;
	}
	
	/**
	 * Execute a dodge action.
	 */
	private executeDodge(): FightActionResult | null{
		return null;
	}
	
	/**
	 * Execute an escape action.
	 */
	private executeEscape(): FightActionResult | null{
		return null;
	}
	
	/**
	 * Execute a change pet action.
	 */
	private executeChangePet(): FightActionResult | null{
		return null;
	}
	
	/**
	 * Execute a use hp potion action.
	 */
	private executeHP(): FightActionResult | null{
		return null;
	}
	
	/**
	 * Execute a use mp potion action.
	 */
	private executeMP(): FightActionResult | null{
		return null;
	}
	
	/**
	 * The character is under the effect of chaos.
	 */
	private executeChaossedAttack(): FightActionResult | null{
		return null;
	}
	
	/**
	 * Create the melee result part of a fight action result.
	 * @param member
	 */
	public static getResultStatus(member: FightMember): FightActionResultStatus{
		let stats = member.base.fightData.stats;
		
		return {
			id: member.base.id,
			totalHp: stats.totalHp,
			hp: stats.hp,
			totalMp: stats.totalMp,
			mp: stats.mp,
			speed: stats.speed,
			effect: member.effect.value,
		};
	}	
	
	/**
	 * Specifies targets for the skill?
	 * @param targets [id, index][]
	 */
	private getDataSkill(targets: [number, number][]): Buffer{
		let data = Buffer.alloc(1 + targets.length * 5);
		data.writeUInt8(targets.length);
		
		for(let i = 0; i < targets.length; ++i){
			let offset = 1 + i * 5;
			data.writeUInt32LE(targets[i][0], offset);
			data.writeUInt8(targets[i][1], offset + 4);
		}
		
		return data;
	}
}
