import { FightActionCommand } from '../../Enums/FightActionCommand';
import { FightEffect } from '../../Enums/FightEffect';
import { getFightActionResult } from '../../Responses/Fight/FightActionResult';
import { fightEndPacket } from '../../Responses/Fight/FightEnd';
import { fightGoPacket } from '../../Responses/Fight/FightGo';
import { getFightStart } from '../../Responses/Fight/FightStart';
import { getFightTurnContinue } from '../../Responses/Fight/FightTurnContinue';
import { fightTurnPausePacket } from '../../Responses/Fight/FightTurnPause';
import { PlayerPackets } from '../../Responses/PlayerPackets';
import { TimedWaiter } from '../../Utils/TimedWaiter';
import type { Game } from '../Game';
import { Monster } from '../Monster/Monster';
import { Player } from '../Player/Player';
import type { FightMember } from './FightMember';

export class Fight{
	/**
	 * Participants in the fight, mapped by ID.
	 */
	public members: Map<number, FightMember> = new Map();
	
	/**
	 * All players in this fight.
	 */
	public players: Set<Player> = new Set();
	
	/**
	 * FightMembers on sideA (location index < 10) sorted by highest speed first.
	 */
	public sideA: FightMember[] = [];
	
	/**
	 * FightMembers on sideB (location index >= 10) sorted by highest speed first.
	 */
	public sideB: FightMember[] = [];
	
	/**
	 * Waits for players to send FightReady.
	 */
	public readyWaiter: TimedWaiter<Player> | null = null;
	
	/**
	 * Waits for players to send FightTurnDone;
	 */
	public turnReadyWaiter: TimedWaiter<Player> | null = null;
	
	/**
	 * Waits for players to send FightEnd.
	 */
	public endWaiter: TimedWaiter<Player> | null = null;
	
	public constructor(
		private game: Game,
	){}
	
	/**
	 * Start the fight.
	 */
	public start(): void{
		this.game.fights.add(this);
		
		for(let member of this.members.values())
			if(member.base instanceof Player)
				this.players.add(member.base);
		
		this.readyWaiter = new TimedWaiter(() => this.onFightReady(), this.players, 20_000);
		this.sendPacket(getFightStart(this));
	}
	
	/**
	 * Fills and sorts the sideA and sideB attributes.
	 */
	public sortBySpeed(): void{
		let members = Array.from(this.members.values());
		
		members.sort((a, b) =>
			// TODO: put speed in FightMember to take speed skill effect into account
			b.base.fightData.stats.speed - a.base.fightData.stats.speed
		);
		
		this.sideA = [];
		this.sideB = [];
		
		for(let member of members){
			if(member.location < 10)
				this.sideA.push(member);
			else
				this.sideB.push(member);
		}
	}
	
	/**
	 * Get the members of one side of the fight.
	 * @param member 
	 * @param opposite
	 */
	public getSide(member: FightMember, opposite: boolean): Readonly<FightMember[]>{
		let sideA = member.location < 10;
		
		if(opposite)
			sideA = !sideA;
		
		return sideA ? this.sideA : this.sideB;
	}
	
	/**
	 * Called when all players are ready.
	 */
	private onFightReady(): void{
		this.sortBySpeed();
		this.sendPacket(fightGoPacket);
		setTimeout(() => this.doNextTurn(this.getNextTurnMember()), 3000);
	}
	
	/**
	 * Called when all players are done with the turn.
	 */
	private onTurnReady(): void{
		if(this.isOneSideDead()){
			this.endFight();
			return;
		}
		
		let turn = this.getNextTurnMember();
		this.sendPacket(getFightTurnContinue(turn.base.id));
		setTimeout(() => this.doNextTurn(turn), 1500);
	}
	
	/**
	 * Called when the fight should be ended.
	 */
	private endFight(): void{
		this.endWaiter = new TimedWaiter(() => this.onClosed(), this.players, 5000);
		this.sendPacket(fightEndPacket);
		
		for(let player of this.players){
			player.enterMap();
			player.client?.write(PlayerPackets.information(player));
			// TODO: 50004 (exp), F0001 (message), 70006 (update money)
		}
	}
	
	/**
	 * Called when all players have called FightClosed.
	 */
	private onClosed(): void{
		this.game.fights.delete(this);
	}
	
	/**
	 * Execute the next turn.
	 */
	private doNextTurn(member: FightMember): void{
		this.sendPacket(fightTurnPausePacket);
		
		member.nextTurn += 1 / member.base.fightData.stats.speed;
		
		if(member.base instanceof Monster)
			this.updateMonsterAction(member);
		
		member.doEffectTurn();
		let result = member.action.execute();
		
		if(!result){
			this.onTurnReady();
			return;
		}
		
		this.turnReadyWaiter = new TimedWaiter(() => this.onTurnReady(), this.players, 5000);
		this.sendPacket(getFightActionResult(result));
	}
	
	/**
	 * Get the fight member whose turn it is.
	 */
	private getNextTurnMember(): FightMember{
		let members = Array.from(this.members.values());
		let turn = members[0];
		
		for(let member of members){
			if(member.effect.has(FightEffect.Dead))
				continue;
			
			if(member.nextTurn < turn.nextTurn)
				turn = member;
		}
		
		return turn;
	}
	
	/**
	 * Update what a monster will do.
	 * @param member
	 */
	private updateMonsterAction(member: FightMember): void{
		let targets = this.getSide(member, true).filter(t => !t.effect.has(FightEffect.Dead));
		let target = targets[Math.floor(Math.random() * targets.length)];
		
		member.action.target = target;
		// TODO mob skills
		member.action.type = FightActionCommand.Melee;
	}
	
	/**
	 * Check if all members on any side have died.
	 */
	private isOneSideDead(): boolean{
		let side1Alive = false;
		let side2Alive = false;
		
		for(let member of this.members.values()){
			if(member.base.fightData.stats.hp === 0)
				continue;
			
			if(member.location < 10)
				side1Alive = true;
			else
				side2Alive = true;
			
			if(side1Alive && side2Alive)
				return false;
		}
		
		return true;
	}
	
	/**
	 * Send packet to all players in the fight.
	 * @param packet
	 */
	private sendPacket(packet: Buffer): void{
		for(let player of this.players)
			player.client?.write(packet);
	}
}
