import { skillProperties } from '../../Data/SkillProperties';
import { FightActionType } from '../../Enums/FightActionType';
import { FightEffect } from '../../Enums/FightEffect';
import { SkillGroup } from '../../Enums/SkillGroup';
import type { SkillData } from '../SkillData';
import { EffectApply } from './EffectApply';
import { FightAction } from './FightAction';
import type { FightActionResult } from './FightActionResultTypes';
import type { FightMember } from './FightMember';
import { SkillTargeting } from './SkillTargeting';

export class FightActionSkill{
	/**
	 * A fightmember uses a skill.
	 * @param action
	 * @param skillData
	 */
	public static execute(action: FightAction, skillData: SkillData): FightActionResult{
		let skill = skillProperties[skillData.id];
		let targets = SkillTargeting.getBySpeed(action);
		
		let result: FightActionResult = {
			type: skill.single ? FightActionType.Magic : FightActionType.MultiMagic,
			source: action.member.base.id,
			target: action.target ? action.target.base.id : 0,
			detail: skill.id,
			status: [FightAction.getResultStatus(action.member)],
			magic: [],
			data: null,
		};
		
		switch(skill.group){
			case SkillGroup.Fire:
			case SkillGroup.Ice:
			case SkillGroup.Evil:
			case SkillGroup.Flash:
				targets.forEach(target => this.executeMagicSkill(result, target, skillData));
				break;
			case SkillGroup.Frailty:
			case SkillGroup.Poison:
			case SkillGroup.Chaos:
			case SkillGroup.Hypnotize:
			case SkillGroup.Stun:
			case SkillGroup.PurgeChaos:
			case SkillGroup.UnStun:
			case SkillGroup.Speed:
			case SkillGroup.Death:
			case SkillGroup.Enhance:
			case SkillGroup.Protect:
			case SkillGroup.Reflect:
			case SkillGroup.Repel:
				targets.forEach(target => this.executeEffectSkill(result, target, skillData));
				break;
			case SkillGroup.MultiShot:
			case SkillGroup.Blizzard:
			case SkillGroup.HealOther:
			case SkillGroup.Drain:
				console.error('Skill not implemented: ' + SkillGroup[skill.group]);
				break;
		}
		
		return result;
	}
	
	/**
	 * Apply fire, ice, evil or flash to the target and update the result.
	 * @param result
	 * @param target
	 * @param skillData
	 */
	private static executeMagicSkill(result: FightActionResult, target: FightMember, skillData: SkillData): void{
		let baseDamage = 50;
		// TODO resist
		let damage = baseDamage;
		// TODO repel
		let repel = 0;
		
		target.base.fightData.stats.addHp(-damage);
		
		result.magic?.push({
			id: target.base.id,
			damage,
			repel,
		});
		
		result.status?.push(FightAction.getResultStatus(target));
	}
	
	/**
	 * Apply effect skills and update the result.
	 * @param result
	 * @param target
	 * @param skillData
	 */
	private static executeEffectSkill(result: FightActionResult, target: FightMember, skillData: SkillData): void{
		// TODO resist
		let chance = .95;
		
		if(Math.random() > chance)
			return;
		
		this.setEffect(target, skillData);
		result.status?.push(FightAction.getResultStatus(target));
	}
	
	/**
	 * Apply the effect of a skill.
	 * @param member
	 * @param skillData
	 */
	private static setEffect(member: FightMember, skillData: SkillData): void{
		let skill = skillProperties[skillData.id];
		
		if(skill.group === SkillGroup.PurgeChaos){
			member.effect.remove(FightEffect.Chaos);
			member.effectCounters.set(FightEffect.Chaos, 0);
			return;
		}
		
		if(skill.group === SkillGroup.UnStun){
			member.effect.remove(FightEffect.Stun);
			member.effectCounters.set(FightEffect.Stun, 0);
			return;
		}
		
		// TODO rounds depend on level
		let rounds = skill.rounds;
		let effect = EffectApply.skillEffectMap[skill.group];
		
		if(!effect)
			return;
		
		let applied = EffectApply.apply(member.effect, effect);
		
		if(applied){
			member.effectCounters.set(effect, rounds);
			member.removeUnusedCounters();
		}
	}
}
