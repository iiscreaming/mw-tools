import type { ClientConnection } from '../ClientConnection';
import { EquipmentSlot } from '../Enums/EquipmentSlot';
import { Skill } from '../Enums/Skill';
import { Item } from './Item/Item';
import { Level } from './Level';
import { Pet } from './Pet/Pet';
import { Player } from './Player/Player';

export class User{
	public characters: Player[] = [];
	
	public constructor(public username: string, client: ClientConnection){
		let test = new Player(client.game, client);
		test.id = 123;
		test.name = 'Test Player';
		test.level = Level.fromExp(100_000);
		test.file = 1;
		test.race = 0;
		test.gender = 0;
		test.mapData.map = client.game.maps.get(1)!;
		test.mapData.x = test.mapData.destX = 475 * 16;
		test.mapData.y = test.mapData.destY = 250 * 8;
		
		test.fightData.stats.sta = 20;
		test.fightData.stats.str = 20;
		test.fightData.stats.int = 20;
		test.fightData.stats.agi = 10;
		
		test.fightData.skills.push({id: Skill.FrailtyI, level: 1, exp: 0});
		test.fightData.skills.push({id: Skill.ChaosI, level: 1, exp: 0});
		test.fightData.skills.push({id: Skill.StunI, level: 1, exp: 0});
		test.fightData.skills.push({id: Skill.HypnotizeI, level: 1, exp: 0});
		test.fightData.skills.push({id: Skill.EvilI, level: 1, exp: 0});
		
		test.fightData.stats.healHp();
		test.fightData.stats.healMp();
		test.fightData.stats.addHpPerc(-50);
		
		test.items.gold = 5000;
		test.items.bankGold = 10000;
		
		let item = new Item(client.game.baseItems.get(1)!);
		test.items.equipment.set(EquipmentSlot.Weapon, item);
		
		let item2 = new Item(client.game.baseItems.get(2)!);
		item2.count = 10;
		test.items.inventory.set(0, item2);
		
		let pet = new Pet();
		pet.id = 0xC0000001;
		pet.name = 'Chicken';
		pet.file = 234;
		pet.growthRate = 1.1;
		pet.fightData.stats.sta = 100;
		pet.fightData.stats.int = 1;
		pet.fightData.stats.str = 10;
		pet.fightData.stats.agi = 1;
		pet.fightData.stats.healHp();
		pet.fightData.stats.healMp();
		pet.fightData.stats.addHpPerc(-50);
		pet.unusedStats = 40;
		pet.loyalty = 100;
		test.pets.push(pet);
		test.activePet = pet;
		
		this.characters.push(test);
	}
}
