import { Species } from '../../Enums/Species';
import { Individual } from '../Individual/Individual';
import { Level } from '../Level';
import { PetFightData } from './PetFightData';
import { PetMapData } from './PetMapData';

// Pet IDs must be in the 0xC0000000 range.
export class Pet extends Individual{
	public fightData: PetFightData = new PetFightData();
	
	public mapData: PetMapData = new PetMapData();
	
	public level: Level = Level.fromLevel(1);
	
	public icon: number = 0;
	
	public effect: number = 0;
	
	public loyalty: number = 0;
	
	public intimacy: number = 0;
	
	public unusedStats: number = 0;
	
	public growthRate: number = 0;
	
	public species: Species = Species.Empty;
}
