module.exports = {
	env: {
		es2021: true,
		node: true,
	},
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 12,
		sourceType: 'module',
	},
	plugins: [
		'@typescript-eslint',
	],
	rules: {
		'linebreak-style': ['warn', 'unix'],
		'prefer-const': ['off'],
		'sort-imports': [
			'warn',
			{
				ignoreDeclarationSort: true,
			},
		],
		
		// ts rules
		'@typescript-eslint/consistent-indexed-object-style': ['warn'],
		'@typescript-eslint/consistent-type-assertions': ['warn'],
		'@typescript-eslint/consistent-type-imports': ['warn'],
		'@typescript-eslint/explicit-function-return-type': ['warn', {
			allowExpressions: true,
		}],
		'@typescript-eslint/explicit-member-accessibility': ['warn'],
		'@typescript-eslint/member-delimiter-style': ['warn', {
			overrides: {
				interface: {
					multiline: {delimiter: 'semi'},
					singleline: {delimiter: 'semi'},
				},
				typeLiteral: {
					multiline: {delimiter: 'comma'},
					singleline: {delimiter: 'comma'},
				},
			},
		}],
		'@typescript-eslint/naming-convention': ['warn',
			{
				selector: 'default',
				format: ['camelCase'],
				leadingUnderscore: 'forbid',
				trailingUnderscore: 'forbid',
			},
			{
				selector: 'variable',
				modifiers: ['const'],
				format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
			},
			{
				selector: 'classProperty',
				modifiers: ['private'],
				format: ['camelCase'],
				leadingUnderscore: 'allow',
			},
			{
				selector: 'classProperty',
				modifiers: ['readonly', 'static'],
				format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
			},
			{
				selector: ['typeLike', 'enumMember'],
				format: ['PascalCase'],
			},
		],
		'@typescript-eslint/no-confusing-non-null-assertion': ['warn'],
		'@typescript-eslint/no-dynamic-delete': ['warn'],
		'@typescript-eslint/no-implicit-any-catch': ['warn'],
		'@typescript-eslint/no-inferrable-types': ['off'],
		'@typescript-eslint/no-invalid-void-type': ['warn'],
		'@typescript-eslint/no-non-null-assertion': ['off'],
		'@typescript-eslint/no-require-imports': ['warn'],
		'@typescript-eslint/no-unnecessary-type-constraint': ['warn'],
		'@typescript-eslint/prefer-enum-initializers': ['warn'],
		'@typescript-eslint/prefer-for-of': ['warn'],
		'@typescript-eslint/prefer-literal-enum-member': ['warn'],
		'@typescript-eslint/prefer-optional-chain': ['warn'],
		'@typescript-eslint/prefer-ts-expect-error': ['warn'],
		'@typescript-eslint/type-annotation-spacing': ['warn'],
		'@typescript-eslint/typedef': ['warn'],
		'@typescript-eslint/unified-signatures': ['warn'],
		
		// eslint rules replaced by ts variant
		'comma-dangle': 'off',
		'comma-spacing': 'off',
		'default-param-last': 'off',
		'func-call-spacing': 'off',
		indent: 'off',
		'keyword-spacing': 'off',
		'no-dupe-class-members': 'off',
		'no-duplicate-imports': 'off',
		'no-empty-function': 'off',
		'no-invalid-this': 'off',
		'no-loop-func': 'off',
		'no-loss-of-precision': 'off',
		'no-redeclare': 'off',
		'no-shadow': 'off',
		'no-unused-expressions': 'off',
		'quotes': 'off',
		'semi': 'off',
		'space-before-function-paren': 'off',
		'space-infix-ops': 'off',
		
		// ts variant
		'@typescript-eslint/comma-dangle': ['warn', {
			arrays: 'always-multiline',
			objects: 'always-multiline',
			imports: 'always-multiline',
			exports: 'always-multiline',
			functions: 'only-multiline',
			enums: 'always-multiline',
			generics: 'always-multiline',
			tuples: 'always-multiline',
		}],
		'@typescript-eslint/comma-spacing': ['warn'],
		'@typescript-eslint/default-param-last': ['warn'],
		'@typescript-eslint/func-call-spacing': ['warn'],
		'@typescript-eslint/indent': ['warn', 'tab', {
			SwitchCase: 1,
			MemberExpression: 'off',
			ignoredNodes: ['TSTypeAliasDeclaration *'],
		}],
		'@typescript-eslint/keyword-spacing': ['warn', {
			before: false,
			after: false,
			overrides: {
				as: {before: true, after: true},
				const: {before: false, after: true},
				export: {before: false, after: true},
				import: {before: false, after: true},
				from: {before: true, after: true},
				let: {before: false, after: true},
				return: {before: false, after: true},
				this: {before: true, after: false},
				case: {before: false, after: true},
				of: {before: true, after: true},
				extends: {before: true, after: true},
			},
		}],
		'@typescript-eslint/no-dupe-class-members': ['warn'],
		'@typescript-eslint/no-duplicate-imports': ['warn'],
		'@typescript-eslint/no-empty-function': ['warn', {
			allow: ['private-constructors', 'protected-constructors', 'decoratedFunctions'],
		}],
		'@typescript-eslint/no-invalid-this': ['warn'],
		'@typescript-eslint/no-loop-func': ['warn'],
		'@typescript-eslint/no-loss-of-precision': ['warn'],
		'@typescript-eslint/no-redeclare': ['warn'],
		'@typescript-eslint/no-shadow': ['warn'],
		'@typescript-eslint/no-unused-expressions': ['warn'],
		'@typescript-eslint/quotes': ['warn', 'single'],
		'@typescript-eslint/semi': ['error'],
		'@typescript-eslint/space-before-function-paren': ['warn', 'never'],
		'@typescript-eslint/space-infix-ops': ['warn'],
	},
	// Rules that require ts type info
	overrides: [
		{
			files: ['*.ts'],
			parserOptions: {
				project: ['./tsconfig.json'],
			},
			rules: {
				'@typescript-eslint/no-base-to-string': ['warn'],
				'@typescript-eslint/no-unnecessary-boolean-literal-compare': ['warn'],
				'@typescript-eslint/no-unnecessary-qualifier': ['warn'],
				'@typescript-eslint/non-nullable-type-assertion-style': ['warn'],
				'@typescript-eslint/prefer-includes': ['warn'],
				'@typescript-eslint/prefer-nullish-coalescing': ['warn'],
				'@typescript-eslint/prefer-string-starts-ends-with': ['warn'],
				'@typescript-eslint/promise-function-async': ['warn'],
				'@typescript-eslint/switch-exhaustiveness-check': ['warn'],
				'dot-notation': 'off',
				'no-throw-literal': 'off',
				'return-await': 'off',
				'@typescript-eslint/dot-notation': ['warn'],
				'@typescript-eslint/no-throw-literal': ['warn'],
				'@typescript-eslint/return-await': ['warn'],
			},
		},
	],
};
